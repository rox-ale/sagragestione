package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Storico;
import it.alessiorossato.sagriamo.services.StoricoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/storico")
public class StoricoController {

    private final Logger log = LoggerFactory.getLogger(StoricoController.class);

    private static final String ENTITY_NAME = "Storico rest";
    @Autowired
    StoricoService storicoService;


    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @GetMapping(value = "/all")
    public ResponseEntity< List<Storico>> customersReport() throws IOException {
        List<Storico> comande =storicoService.allComanda();



        //  HttpHeaders headers = new HttpHeaders();
        //headers.add("Content-Disposition", "inline; filename=customers.pdf");
        //   List<it.alessiorossato.sagriamo.bean.Comanda> convertito = ConvertModelBean.convertiComande(comande);





        /*
        prova per lista utenti
         */
       /* Object principal = SecurityContextHolder.getContext().getAuthentication().;
        String username;
        if (principal instanceof UserDetails) {
              username = ((UserDetails)principal).getUsername();
        } else {
              username = principal.toString();
        }

        System.out.println("Nome "+((UserDetails)principal).);*/

        List<Object> principals = sessionRegistry.getAllPrincipals();



        for (Object principal: principals) {

                System.out.println((((User) principal).getUsername()));

        }
        System.out.println("Lista "+principals.toString());
        return new ResponseEntity< List<Storico>>(comande, HttpStatus.OK);
    }


    @GetMapping(value = "/cassa")
    public ResponseEntity< List<Storico>> cassaComande(@RequestParam Long idCassa) throws IOException {
        List<Storico> comande =storicoService.allComandaCassa(idCassa);


        return new ResponseEntity< List<Storico>>(comande, HttpStatus.OK);
    }
}
