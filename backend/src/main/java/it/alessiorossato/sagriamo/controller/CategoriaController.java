package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Categoria;
import it.alessiorossato.sagriamo.services.CategoriaService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/categoria")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    private static final String ENTITY_NAME = "Categoria";


    @RequestMapping(value = "/listaCategorie", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Categoria>> listaCategorie() {
        return  new ResponseEntity<Set<Categoria>>(ConvertModelBean.convertiCategoria(categoriaService.listaCategorie()), HttpStatus.OK);
    }

    @PostMapping("/inserisci")
    public ResponseEntity<Set<Categoria>> aggiornaCategoria(@RequestBody Categoria[] categorie) throws URISyntaxException {


        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto categorie: " + categorie);
        Set <it.alessiorossato.sagriamo.model.Categoria> lista=new HashSet<>();
        for(Categoria c:categorie){
            it.alessiorossato.sagriamo.model.Categoria cc= ConvertBeanModel.convertiCategoria(c);
            lista.add(cc);
        }

        List<it.alessiorossato.sagriamo.model.Categoria> result = categoriaService.salva(lista);
        Set<Categoria> resultBean = ConvertModelBean.convertiCategoria(result);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(resultBean);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }

}
