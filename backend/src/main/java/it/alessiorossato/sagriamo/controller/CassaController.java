package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Cassa;
import it.alessiorossato.sagriamo.bean.Categoria;
import it.alessiorossato.sagriamo.services.CassaService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/cassa")
public class CassaController {

    private static final String ENTITY_NAME = "Cassa";
    @Autowired
    private CassaService cassaService;

    @Autowired
    SessioneService sessioneService;

    @RequestMapping(value = "/listaCasse", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Cassa>> casseAttive() {
        if (sessioneService.verificaSessioneAttiva()) {
            return new ResponseEntity<Set<Cassa>>(ConvertModelBean.convertiCassa(cassaService.listacasse()), HttpStatus.OK);

        } else {
        //    return new ResponseEntity(HttpStatus.LOCKED);
            //se la sessione non è attiva ritorno unalista vuota
            Set<Cassa> listacasse=new HashSet<>();

            return new ResponseEntity<Set<Cassa>>(listacasse, HttpStatus.OK);

        }
    }

    @RequestMapping(value = "/listaProdotto", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Categoria>> categorieAttive(@RequestParam boolean bar) {
        if (sessioneService.verificaSessioneAttiva()) {
            return new ResponseEntity<Set<Categoria>>(ConvertModelBean.convertiListaProdotti(cassaService.listaProdotti(bar)), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }
    }

    @RequestMapping(value = "/listaCasseTutte", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Set<Cassa>> listaCasseTutte() {
        return new ResponseEntity<Set<Cassa>>(ConvertModelBean.convertiCassa(cassaService.listaCasseTutte()), HttpStatus.OK);
    }


    @PostMapping("/inserisci")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Set<Cassa>> aggiornaCasse(@RequestBody Cassa[] casse) throws URISyntaxException {
        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto casse: " + casse);
        Set<it.alessiorossato.sagriamo.model.Cassa> lista = new HashSet<>();
        for (Cassa c : casse) {
            it.alessiorossato.sagriamo.model.Cassa cc = ConvertBeanModel.convertiCassa(c);
            lista.add(cc);
        }

        List<it.alessiorossato.sagriamo.model.Cassa> result = cassaService.salva(lista);

        Set<Cassa> resultBean =
                new HashSet<>();

        for (it.alessiorossato.sagriamo.model.Cassa c : result) {
            resultBean.add(ConvertModelBean.convertiCassa(c));
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(resultBean);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }


    @RequestMapping(value = "/prova423", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity listaProva423() {
        System.out.println("richiesta 423");
        return new ResponseEntity(HttpStatus.LOCKED);
    }
}
