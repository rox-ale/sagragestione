package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Stampante;
import it.alessiorossato.sagriamo.services.StampanteService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/stampante")

public class StampanteController {

    @Autowired
    StampanteService stampanteService;

    private final Logger log = LoggerFactory.getLogger(StampanteController.class);

    private static final String ENTITY_NAME = "Stampante rest";

    @RequestMapping(value="/listaStampanti",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Stampante>> listaStampanti ()  {


        log.debug("REST request to get stampanti  : {}");

        Set<it.alessiorossato.sagriamo.model.Stampante> lista=   stampanteService.listaStampanti();

        Set<Stampante> convertito=new HashSet<>();
        for ( it.alessiorossato.sagriamo.model.Stampante s:lista){
            Stampante stampante= ConvertModelBean.convertiStampante(s);
            convertito.add(stampante);
        }
        return new ResponseEntity<Set<Stampante>>(convertito, HttpStatus.OK);
    }





    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/inserisci")
    public ResponseEntity<Set<Stampante>> inserisciStampanti(@RequestBody Stampante[] stampanti) throws URISyntaxException {


        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto stampanti: " + stampanti);
        Set <it.alessiorossato.sagriamo.model.Stampante> lista=new HashSet<>();
        for(Stampante s:stampanti){
            it.alessiorossato.sagriamo.model.Stampante cc= ConvertBeanModel.convertiStampante(s);
            lista.add(cc);
        }

        List<it.alessiorossato.sagriamo.model.Stampante> result = stampanteService.salva(lista);
        Set<Stampante> resultBean =new HashSet<>();
        for(it.alessiorossato.sagriamo.model.Stampante s :result){
            resultBean.add(ConvertModelBean.convertiStampante(s));
        }

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(resultBean);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }
}
