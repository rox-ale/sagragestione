package it.alessiorossato.sagriamo.controller;


import it.alessiorossato.sagriamo.model.DettaglioComanda;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.pdf.PDFGeneratorA5;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.DettaglioComandaDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.HashSet;
import java.util.Set;
import java.util.Optional;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/pdf")
public class PdfController {

    private final Logger log = LoggerFactory.getLogger(PdfController.class);

    private static final String ENTITY_NAME = "Pdf rest";
    @Autowired
    ComandaDAO comandaDAO;

    @Autowired
    DettaglioComandaDAO dettaglioComandaDAO;

    @Autowired
    PDFGeneratorA5 pdfGeneratorA5;

    @GetMapping(value = "/pdfa5",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> customersReporta5(Long id) throws IOException {
        //Optional<Comanda> c=comandaRepository.findById(new Long(33));

        Optional <Comanda>  c=comandaDAO.findById(id);
     //   PDFGeneratorA5 pa5=new PDFGeneratorA5();
        //   ByteArrayInputStream bis = PDFGenerator.pdfCliente(customers);
        Comanda cc=c.get();
        Set<DettaglioComanda> h=dettaglioComandaDAO.prodottiComanda(cc.getId());
        cc.setDettaglioComandas(h);

        ByteArrayInputStream bis=pdfGeneratorA5.pdfCliente(cc);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=customers.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @GetMapping(value = "/pdfCucinaA5",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> pdfCucinaA5(Long id) throws IOException {
        //Optional<Comanda> c=comandaRepository.findById(new Long(33));
        Optional<Comanda> c=comandaDAO.findById(id);
      //  PDFGeneratorA5 pa5=new PDFGeneratorA5();
        //   ByteArrayInputStream bis = PDFGenerator.pdfCliente(customers);
        Comanda cc=c.get();
        Set<DettaglioComanda> h=dettaglioComandaDAO.prodottiComanda(cc.getId());
        cc.setDettaglioComandas(h);

        Set <Long> listaReparti=new HashSet<>();
        listaReparti.add(2L);
        ByteArrayInputStream bis=pdfGeneratorA5.pdfReparto(cc,listaReparti);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=customers.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

}
