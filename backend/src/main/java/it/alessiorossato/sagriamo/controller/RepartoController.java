package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Cassa;
import it.alessiorossato.sagriamo.bean.Categoria;
import it.alessiorossato.sagriamo.bean.Reparto;
import it.alessiorossato.sagriamo.bean.Stampante;
import it.alessiorossato.sagriamo.services.CassaService;
import it.alessiorossato.sagriamo.services.RepartoService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/reparto")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class RepartoController {
    private final Logger log = LoggerFactory.getLogger(RepartoController.class);

    private static final String ENTITY_NAME = "reparto";

    @Autowired
    RepartoService repartoService;

    @RequestMapping(value = "/listaReparti", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reparto>> listaReparti() {


        log.debug("REST request to get stampanti  : {}");

        List<it.alessiorossato.sagriamo.model.Reparto> lista = repartoService.listaReaprti();

        List<Reparto> convertito = new ArrayList<>();
        for (it.alessiorossato.sagriamo.model.Reparto r : lista) {
            Reparto reparto = ConvertModelBean.convertiReparto(r);
            convertito.add(reparto);
        }
        return new ResponseEntity<List<Reparto>>(convertito, HttpStatus.OK);
    }

    @PostMapping("/inserisci")
    public ResponseEntity<Set<Reparto>> aggiornaReparti(@RequestBody Reparto[] reparti) throws URISyntaxException {


        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto casse: " + reparti);
        Set<it.alessiorossato.sagriamo.model.Reparto> lista = new HashSet<>();
        for (Reparto r : reparti) {
            it.alessiorossato.sagriamo.model.Reparto rr = ConvertBeanModel.convertiReparto(r);
            lista.add(rr);
        }

        List<it.alessiorossato.sagriamo.model.Reparto> result = repartoService.salva(lista);

        Set<it.alessiorossato.sagriamo.bean.Reparto> resultBean = new HashSet<>();

        for (it.alessiorossato.sagriamo.model.Reparto r : result) {
            resultBean.add(ConvertModelBean.convertiReparto(r));
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(resultBean);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }


}
