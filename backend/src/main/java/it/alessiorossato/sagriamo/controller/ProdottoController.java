package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Prodotto;
import it.alessiorossato.sagriamo.services.ProdottoService;
import it.alessiorossato.sagriamo.services.RicettaService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/prodotto")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ProdottoController {

    @Autowired
    ProdottoService prodottoService;

    @Autowired
    RicettaService ricettaService;

    private final Logger log = LoggerFactory.getLogger(ProdottoController.class);

    private static final String ENTITY_NAME = "Prodotto rest";

    @RequestMapping(value="/listaProdotti",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Prodotto>> listaProdotti ()  {


       // log.debug("REST request to get stampanti  : {}");

        List<it.alessiorossato.sagriamo.model.Prodotto> prodottos = prodottoService.listaProdotti();

        Set <Prodotto> lista=new HashSet<>();
        for(it.alessiorossato.sagriamo.model.Prodotto p:prodottos){
            lista.add(ConvertModelBean.convertiProdotto(p));
        }
        return new ResponseEntity<Set<Prodotto>>(lista, HttpStatus.OK);
    }


    /**
     * salva una lista di prodotti con tutta la relativa ricetta
     * @param prodotti
     * @return
     */
    @PostMapping("/inserisci")
    public ResponseEntity<Set<Prodotto>> aggiornaProdotto(@RequestBody Prodotto[] prodotti)  {


        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto categorie: " + prodotti);
        Set <it.alessiorossato.sagriamo.model.Prodotto> lista=new HashSet<>();
        for(Prodotto c:prodotti){
            it.alessiorossato.sagriamo.model.Prodotto cc= ConvertBeanModel.convertiProdottoCategoriaRicetta(c);
            lista.add(cc);
        }

        List<it.alessiorossato.sagriamo.model.Prodotto> result = prodottoService.salva(lista);

        for(it.alessiorossato.sagriamo.model.Prodotto prodotto:lista){
           ricettaService.salva(prodotto.getRicetta());
           ricettaService.eliminaRicettaEliminatiIngredienti(prodotto.getRicetta(),prodotto);
           prodotto.setRicetta(ricettaService.listaRicettaProdotto(prodotto.getId()));
        }



        List<it.alessiorossato.sagriamo.model.Prodotto> prodottos = prodottoService.listaProdotti();

        Set <Prodotto> listaN=new HashSet<>();
        for(it.alessiorossato.sagriamo.model.Prodotto p:prodottos){
            p.setRicetta(ricettaService.listaRicettaProdotto(p.getId()));
            listaN.add(ConvertModelBean.convertiProdotto(p));
        }

        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(listaN);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }
}
