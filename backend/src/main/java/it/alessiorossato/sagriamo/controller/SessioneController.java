package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.Cassa;
import it.alessiorossato.sagriamo.bean.Categoria;
import it.alessiorossato.sagriamo.bean.Sessione;
import it.alessiorossato.sagriamo.services.CassaService;
import it.alessiorossato.sagriamo.services.GriglieService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/sessione")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class SessioneController {

    private static final String ENTITY_NAME = "Cassa";

    @Autowired
    SessioneService sessioneService;

    @Autowired
    GriglieService griglieService;

    @RequestMapping(value = "/listaSessioni", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Sessione>> listaSessioni() {
        return new ResponseEntity<Set<Sessione>>(ConvertModelBean.convertiSessione(sessioneService.listaSessioni()), HttpStatus.OK);
    }


    @RequestMapping(value = "/chiudiSessione", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Sessione>> chiudiSessione() {
        sessioneService.chiudiSessioni();
        griglieService.svuotaDatiGriglia();
        return new ResponseEntity<Set<Sessione>>(ConvertModelBean.convertiSessione(sessioneService.listaSessioni()), HttpStatus.OK);

    }

    @RequestMapping(value = "/apriSessione", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Sessione>> apriSessione() {
        sessioneService.apriSessione();
        return new ResponseEntity<Set<Sessione>>(ConvertModelBean.convertiSessione(sessioneService.listaSessioni()), HttpStatus.OK);

    }
}
