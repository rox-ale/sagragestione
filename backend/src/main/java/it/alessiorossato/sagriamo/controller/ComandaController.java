package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.*;
import it.alessiorossato.sagriamo.pdf.PDFGeneratorA5;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.DettaglioComandaDAO;
import it.alessiorossato.sagriamo.services.*;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import it.alessiorossato.sagriamo.util.Stampa;
import it.alessiorossato.sagriamo.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/comanda")
public class ComandaController {
    private final Logger log = LoggerFactory.getLogger(ComandaController.class);

    private static final String ENTITY_NAME = "Gestione comanda";

    @Autowired
    ComandaDAO comandaDAO;
    @Autowired
    DettaglioComandaDAO dettaglioComandaDAO;

    @Autowired
    ComandaService comandaService;

    @Autowired
    SessioneService sessioneService;

    @Autowired
    OpzioneService opzioneService;


    @Autowired
    RepartoService repartoService;

    @Autowired
    ProdottoService prodottoService;

    @Autowired
    PDFGeneratorA5 pdfGeneratorA5;

    @PostMapping("/inserisci")
    public ResponseEntity<it.alessiorossato.sagriamo.model.Comanda> creaComanda(@RequestBody ComandaStampante comandaStampante) throws URISyntaxException {

        if (sessioneService.verificaSessioneAttiva()) {
            Comanda comanda = comandaStampante.getComanda();
            Stampante s = comandaStampante.getStampante();//non ancora uttilizzato

            // Comanda comanda=new Comanda();
            System.out.println("Ricevuto comanda: " + comanda);
            System.out.println("Cassa" + comanda.getCassa().toString());
            System.out.println("Dettaglio comanda " + comanda.getDettaglioComandas().toString());
            log.debug("REST request to save Comanda : {}", comanda);
            if (comanda.getId() != null) {
                // throw new BadRequestAlertException("A new cassa cannot already have an ID", ENTITY_NAME, "idexists");
            }

            LocalDateTime ldt = Utility.dataConfronto;// LocalDateTime.of(2019, Month.MAY,  5,   10,   5);

            comanda.setDataOra(LocalDateTime.now());
            comanda.setPreparazione( ldt);
            comanda.setRitirato( ldt);
            comanda.setConsegnato( ldt);


            String progressivo=opzioneService.getProgressivo();
            comanda.setProgressivo(Integer.parseInt(progressivo));

            it.alessiorossato.sagriamo.model.Comanda result = comandaDAO.save(ConvertBeanModel.convertiComanda(comanda));

            System.out.println("ID comanda: " + result.getId());
            for (DettaglioComanda d : comanda.getDettaglioComandas()) {
                // d.setComanda(ConvertModelBean.convertiComanda(result));
                //System.out.println("Converti comanda "+ConvertBeanModel.convertiDettaglioComanda(d));
                it.alessiorossato.sagriamo.model.DettaglioComanda dd = ConvertBeanModel.convertiDettaglioComanda(d);
                dd.setComanda(result);
                dd.setProdotto(ConvertBeanModel.convertiProdotto(d.getProdotto()));
                dd.setTime(LocalDateTime.now());
                dettaglioComandaDAO.save(dd);
                result.getDettaglioComandas().add(dd);
            }

          //  PDFGeneratorA5 pdfGeneratorA5 = new PDFGeneratorA5();
            Stampa cliente=new Stampa(pdfGeneratorA5.pdfCliente(result),comandaStampante.getStampante().getNomeStampantePc());
            cliente.start();
/*
for(it.alessiorossato.sagriamo.model.DettaglioComanda dc:result.getDettaglioComandas()){
    it.alessiorossato.sagriamo.model.Categoria categoriaProdotto = prodottoService.categoriaProdotto(dc.getProdotto());
    //categoriaProdotto.setProdottos(null);
    //categoriaProdotto.getReparto().setCategoria(null);
    //categoriaProdotto.getReparto().setStampante(null);
   dc.getProdotto().setCategoria(categoriaProdotto);
}*/

            //stampo per ogni reparto abilitato
        List<it.alessiorossato.sagriamo.model.Reparto> listaReaprti = repartoService.listaReaprti();
            for(it.alessiorossato.sagriamo.model.Reparto r:listaReaprti){
                if(r.getAbilitato()){
                    Set<Long> listaRepartidaStampareInComanda=new HashSet<>();
                    listaRepartidaStampareInComanda.add(Long.valueOf(r.getId()));
                    ByteArrayInputStream pdfReparto = pdfGeneratorA5.pdfReparto(result, listaRepartidaStampareInComanda);
                   Stampa repartoStampa=new Stampa(pdfReparto,r.getStampante().getNomeStampantePc());
                    repartoStampa.start();
                }
            }


            it.alessiorossato.sagriamo.model.Comanda returnComanda= new it.alessiorossato.sagriamo.model.Comanda();
            returnComanda.setId(result.getId());
            returnComanda.setProgressivo(result.getProgressivo());
            returnComanda.setTavolo(result.getTavolo());
            returnComanda.setResto(result.getResto());
            returnComanda.setTotale(result.getTotale());
            returnComanda.setVersato(result.getVersato());

            return ResponseEntity.created(new URI("/comanda/inserisci/" ))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, ""))
                    .body(returnComanda);
          //return null;
        } else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }



    }


    @PostMapping("/elimina")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?>     eliminaComanda(@RequestBody Long id) throws URISyntaxException {
        comandaService.eliminaComanda(id);

        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

    }
}
