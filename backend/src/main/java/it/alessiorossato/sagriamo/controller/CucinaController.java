package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.services.CucinaService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/cucina")
public class CucinaController {

    private final Logger log = LoggerFactory.getLogger(CucinaController.class);

    private static final String ENTITY_NAME = "Cucina rest";

    @Autowired
    CucinaService cucinaService;

    @Autowired
    SessioneService sessioneService;

    @GetMapping(value = "/dafare")
    public ResponseEntity<Set<it.alessiorossato.sagriamo.bean.Comanda>> dafare() {
        if (sessioneService.verificaSessioneAttiva()) {
            Set<Comanda> comande = cucinaService.allComandaDaFare();


            //  HttpHeaders headers = new HttpHeaders();
            //headers.add("Content-Disposition", "inline; filename=customers.pdf");
            Set<it.alessiorossato.sagriamo.bean.Comanda> convertito = ConvertModelBean.convertiComande(comande);

            return new ResponseEntity<Set<it.alessiorossato.sagriamo.bean.Comanda>>(convertito, HttpStatus.OK);
        }
        else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }
    }

    @GetMapping(value = "/presaincarico")
    public ResponseEntity<?>   presaInCarico(@RequestParam Long id) {

        if (sessioneService.verificaSessioneAttiva()) {
            cucinaService.comandaPresaInCarico(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }


    }




    @GetMapping(value = "/indistribuzione")
    public ResponseEntity<?>   inDistribuzione(@RequestParam Long id) {


        if (sessioneService.verificaSessioneAttiva()) {
            cucinaService.comandaInDistribuzione(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }

    }

    @GetMapping(value = "/ritirata")
    public ResponseEntity<?>   ritirata(@RequestParam Long id) {


        if (sessioneService.verificaSessioneAttiva()) {
            cucinaService.comandaRitirata(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

        }
        else {
            return new ResponseEntity(HttpStatus.LOCKED);
        }
    }
}
