package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.*;
import it.alessiorossato.sagriamo.model.Ricetta;
import it.alessiorossato.sagriamo.repository.RicettaDAO;
import it.alessiorossato.sagriamo.services.IngredienteService;
import it.alessiorossato.sagriamo.util.ConvertBeanModel;
import it.alessiorossato.sagriamo.util.ConvertModelBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/api/ingrediente")
public class IngredienteController {
    private final Logger log = LoggerFactory.getLogger(IngredienteController.class);

    private static final String ENTITY_NAME = "Gestione ingrediente";

    @Autowired
    IngredienteService ingredienteService;

    @Autowired
    RicettaDAO ricettaDAO;

    @RequestMapping(value = "/listaIngredienti", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Ingrediente>> listaIngredienti() {
        List<Ricetta> all = ricettaDAO.findAll();
        System.out.println("Ricetta ");
    for(Ricetta a:all){
        System.out.println(a);
    }

        return  new ResponseEntity<Set<Ingrediente>>(ConvertModelBean.convertiIngrediente(ingredienteService.listaIngredienti()), HttpStatus.OK);
    }



    @PostMapping("/inserisci")
    public ResponseEntity<Set<Ingrediente>> aggiornaIngrediente(@RequestBody Ingrediente[] ingrediente) throws URISyntaxException {


        // Comanda comanda=new Comanda();
        System.out.println("Ricevuto categorie: " + ingrediente);
        Set <it.alessiorossato.sagriamo.model.Ingrediente> lista=new HashSet<>();
        for(Ingrediente i:ingrediente){
            it.alessiorossato.sagriamo.model.Ingrediente in= ConvertBeanModel.convertiIngrediente(i);
            lista.add(in);
        }

        List<it.alessiorossato.sagriamo.model.Ingrediente> result = ingredienteService.salva(lista);
        Set<Ingrediente> resultBean = ConvertModelBean.convertiIngrediente(result);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
                .body(resultBean);
        //  return    ResponseEntity.ok(ConvertBeanModel.convertiComanda(comanda));

    }

}
