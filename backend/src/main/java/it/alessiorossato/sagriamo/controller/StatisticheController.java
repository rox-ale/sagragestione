package it.alessiorossato.sagriamo.controller;

import it.alessiorossato.sagriamo.bean.TotaleCassa;
import it.alessiorossato.sagriamo.bean.TotaleProdotto;
import it.alessiorossato.sagriamo.bean.TotaleSessioneCassa;
import it.alessiorossato.sagriamo.bean.TotaleSessioneProdotto;
import it.alessiorossato.sagriamo.model.*;
import it.alessiorossato.sagriamo.pdf.PDFGeneratorA5;
import it.alessiorossato.sagriamo.pdf.PDFStatistiche;
import it.alessiorossato.sagriamo.repository.query.IProdottoTotale;
import it.alessiorossato.sagriamo.repository.query.ProdottoTotale;
import it.alessiorossato.sagriamo.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/statistiche")
public class StatisticheController {

    private final Logger log = LoggerFactory.getLogger(StatisticheController.class);

    private static final String ENTITY_NAME = "Storico rest";

    @Autowired
    ComandaService comandaService;

    @Autowired
    CassaService cassaService;

    @Autowired
    SessioneService sessioneService;

    @Autowired
    ProdottoService prodottoService;

    @Autowired
    DettaglioComandaService dettaglioComandaService;

    @Autowired
    PDFStatistiche pdfStatistiche;

    @RequestMapping(value = "/statistiche", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<TotaleSessioneCassa>> listaStatistiche() {
        Set<TotaleSessioneCassa> totale = new HashSet<>();

        //Sessione Totale sagra

        TotaleSessioneCassa totaleSagra = new TotaleSessioneCassa();
        totaleSagra.setNome("Totale sagra");

        Set<Cassa> listaCasseTutte = cassaService.listaCasseTutte();
        double totaleImportosagra = 0;
        for (Cassa cassa : listaCasseTutte) {
            double totaleIncassoCassa = comandaService.totaleCassa(cassa);
            TotaleCassa totaleCassa = new TotaleCassa();
            totaleCassa.setNome(cassa.getNome());
            totaleCassa.setTotaleCassa(totaleIncassoCassa);
            totaleSagra.getTotaleCasse().add(totaleCassa);
            totaleImportosagra += totaleIncassoCassa;
        }
        totaleSagra.setTotale(totaleImportosagra);

        totale.add(totaleSagra);


        //totali sessioni

        /*
        calcolo il toale per ogni sessione e per ogni cassa
         */
        Set<Sessione> listaSessioni = sessioneService.listaSessioni();

        for (Sessione s : listaSessioni) {
            TotaleSessioneCassa totaleSessione = new TotaleSessioneCassa();
         if(s.getDataFine()!=null){
             totaleSessione.setNome(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+"  -  "+
                     s.getDataFine().getYear()+"/"+s.getDataFine().getMonthValue()+"/"+ s.getDataFine().getDayOfMonth() +"  "+s.getDataFine().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())
             );
         }
           else{
               totaleSessione.setNome(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+ "null"
             );
         }

            listaCasseTutte = cassaService.listaCasseTutte();
            double totaleImporto = 0;
            for (Cassa cassa : listaCasseTutte) {
                double totaleIncassoCassa=0;
                if(s.getDataFine()==null){
                    totaleIncassoCassa    = comandaService.totaleCassa(cassa, s.getDataInizio());
                }else{
                    totaleIncassoCassa    = comandaService.totaleCassa(cassa, s.getDataInizio(), s.getDataFine());
                }

                TotaleCassa totaleCassa = new TotaleCassa();
                totaleCassa.setNome(cassa.getNome());
                totaleCassa.setTotaleCassa(totaleIncassoCassa);
                totaleSessione.getTotaleCasse().add(totaleCassa);
                totaleImporto += totaleIncassoCassa;
            }


            totaleSessione.setTotale(totaleImporto);
            totale.add(totaleSessione);
        }


        return new ResponseEntity<Set<TotaleSessioneCassa>>((totale), HttpStatus.OK);
    }






    @RequestMapping(value = "/statisticheProdotti", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<TotaleSessioneProdotto>> listaStatisticheProdotti() {
        Set<TotaleSessioneProdotto> totale = new HashSet<>();

        //Sessione Totale sagra prodotti

        TotaleSessioneProdotto totaleSagra = new TotaleSessioneProdotto();
        totaleSagra.setNome("Totale sagra");

         Collection<IProdottoTotale> listaProdottiTutti = prodottoService.totaleProdotti();


        for (IProdottoTotale prodotto : listaProdottiTutti) {
          //  System.out.println("prodotto "+prodotto);

            int totaleNumeroProdotti = prodotto.getTotale();
            TotaleProdotto totaleProdotto = new TotaleProdotto();
            totaleProdotto.setNome(prodotto.getNome());
            totaleProdotto.setTotaleProdotto(totaleNumeroProdotti);
            totaleSagra.getTotaleProdotto().add(totaleProdotto);


            System.out.println("Totale prodotto "+totaleProdotto);
        }


        totale.add(totaleSagra);


        //totali sessioni

        /*
        calcolo il toale per ogni sessione e per ogni cassa
         */
        Set<Sessione> listaSessioni = sessioneService.listaSessioni();
        listaProdottiTutti=null;
        for (Sessione s : listaSessioni) {
            TotaleSessioneProdotto totaleSessione = new TotaleSessioneProdotto();
            if(s.getDataFine()!=null){
                totaleSessione.setNome(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+"  -  "+
                        s.getDataFine().getYear()+"/"+s.getDataFine().getMonthValue()+"/"+ s.getDataFine().getDayOfMonth() +"  "+s.getDataFine().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())
                );
                listaProdottiTutti=prodottoService.totaleCassa(s.getDataInizio(),s.getDataFine());
            }
            else{
                totaleSessione.setNome(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+ "null"
                );
                listaProdottiTutti=prodottoService.totaleCassa(s.getDataInizio());
            }

            int tavoli=0,tavoliOmaggio=0,asporto=0,asportoOmaggio=0;

            Set<Comanda> comandeSessione = comandaService.comandeSessione(s);

            /*
            scorro tutte le comande per decidere come mettere i totali dei coperti
             */

            for(Comanda c:comandeSessione){
                if(c.getTavolo()){
                    tavoli+=c.getCoperti();
                    tavoliOmaggio+=c.getOmaggio();
                }else{
                    asporto+=c.getCoperti();
                    asportoOmaggio+=c.getOmaggio();
                }
            }

            String totaliCoperti="Totale coperti serata: "+(tavoli+tavoliOmaggio+asporto+asportoOmaggio)+" Tavoli: "+tavoli+" + Tavoli Omaggio "+tavoliOmaggio+" + Asporto: "+asporto+" + Asporto Omaggio "+asportoOmaggio;
            totaleSessione.setTotali(totaliCoperti);


            for (IProdottoTotale prodotto : listaProdottiTutti) {



                int totaleNumeroProdotti = prodotto.getTotale();
                TotaleProdotto totaleProdotto = new TotaleProdotto();
                totaleProdotto.setNome(prodotto.getNome());
                totaleProdotto.setTotaleProdotto(totaleNumeroProdotti);
                totaleSessione.getTotaleProdotto().add(totaleProdotto);
            }



            totale.add(totaleSessione);
        }

        return new ResponseEntity<Set<TotaleSessioneProdotto>>((totale), HttpStatus.OK);

    }




    @GetMapping(value = "/scaricaStatistiche",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> customersReporta5() throws IOException {

        //PDFStatistiche pdfS=new PDFStatistiche();


       // ByteArrayInputStream bis=pdfS.pdfStatistiche();

        ByteArrayInputStream bis=pdfStatistiche.pdfStatistiche();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=pdfStatistiche.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
