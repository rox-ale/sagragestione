package it.alessiorossato.sagriamo.pdf;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import it.alessiorossato.sagriamo.bean.TotaleCassa;
import it.alessiorossato.sagriamo.bean.TotaleProdotto;
import it.alessiorossato.sagriamo.bean.TotaleSessioneProdotto;
import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.DettaglioComanda;
import it.alessiorossato.sagriamo.model.Sessione;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.ProdottoDAO;
import it.alessiorossato.sagriamo.repository.query.IProdottoTotale;
import it.alessiorossato.sagriamo.services.CassaService;
import it.alessiorossato.sagriamo.services.ComandaService;
import it.alessiorossato.sagriamo.services.ProdottoService;
import it.alessiorossato.sagriamo.services.SessioneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@Service
public class PDFStatistiche {
    private static Logger logger = LoggerFactory.getLogger(PDFStatistiche.class);

    @Autowired
    ProdottoService prodottoService;

    @Autowired
    CassaService cassaService;

    @Autowired
    ComandaService comandaService;

    @Autowired
    SessioneService sessioneService;

    public ByteArrayInputStream pdfStatistiche() {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            PdfWriter.getInstance(document, out);
            document.setPageSize(PageSize.A4);//aggiunto io
            document.setMargins(10, 10, 10, 10);
            document.open();

            float fntSize, lineSpacing;
            fntSize = 40f;
            lineSpacing = 25f;
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, fntSize, Font.BOLD);
            Paragraph p = new Paragraph(new Phrase(lineSpacing, "Statistiche sagra",
                    boldFont));
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);

            Font boldFontM = new Font(Font.FontFamily.TIMES_ROMAN, fntSize / 2 + 5, Font.BOLD);
            p = new Paragraph(new Phrase(lineSpacing + 20, "Incassi",
                    boldFontM));

            document.add(p);
            Font boldFontTotale = new Font(Font.FontFamily.TIMES_ROMAN, fntSize / 3, Font.BOLD);
            p = new Paragraph(new Phrase(lineSpacing - 5, "Totali sagra",
                    boldFontTotale));
            document.add(p);
//tabella totale sagra incassi
            PdfPTable tableTotaleIncassoSagra = new PdfPTable(1);
            PdfPCell sn = null;


            Set<Cassa> listaCasseTutte = cassaService.listaCasseTutte();
            double totaleImportosagra = 0;
            for (Cassa cassa : listaCasseTutte) {
                double totaleIncassoCassa = comandaService.totaleCassa(cassa);

                totaleImportosagra += totaleIncassoCassa;

                sn = new PdfPCell(new Phrase(cassa.getNome() + " " + totaleIncassoCassa));
                tableTotaleIncassoSagra.addCell(sn);

            }

            sn = new PdfPCell(new Phrase("Totale sagra: " + totaleImportosagra, boldFontM));
            sn.setBorderColor(BaseColor.BLACK);
            tableTotaleIncassoSagra.addCell(sn);
            tableTotaleIncassoSagra.setSpacingBefore(10f);
            document.add(tableTotaleIncassoSagra);


            //totali sessioni

        /*
        calcolo il toale per ogni sessione e per ogni cassa
         */
            Set<Sessione> listaSessioni = sessioneService.listaSessioni();
            Font boldFontSessione = new Font(Font.FontFamily.TIMES_ROMAN, fntSize / 2, Font.BOLD);
            for (Sessione s : listaSessioni) {
                System.out.println("Sessione " + s);
                String nomeSessione;
                if (s.getDataFine() != null) {
                    nomeSessione = s.getDataInizio().getYear() + "/" + s.getDataInizio().getMonthValue() + "/" + s.getDataInizio().getDayOfMonth() + "  " + s.getDataInizio().getHour() + "." + String.format("%02d", s.getDataInizio().getMinute()) + "  -  " +
                            s.getDataFine().getYear() + "/" + s.getDataFine().getMonthValue() + "/" + s.getDataFine().getDayOfMonth() + "  " + s.getDataFine().getHour() + "." + String.format("%02d", s.getDataInizio().getMinute());
                } else {
                    nomeSessione = (s.getDataInizio().getYear() + "/" + s.getDataInizio().getMonthValue() + "/" + s.getDataInizio().getDayOfMonth() + "  " + s.getDataInizio().getHour() + "." + String.format("%02d", s.getDataInizio().getMinute()) + "  -  Adesso"
                    );
                }
                p = new Paragraph(new Phrase(lineSpacing - 5, nomeSessione,
                        boldFontTotale));
                document.add(p);


                PdfPTable tableTotaleSessione = new PdfPTable(1);
                sn = null;


                listaCasseTutte = cassaService.listaCasseTutte();
                double totaleImporto = 0;
                for (Cassa cassa : listaCasseTutte) {
                    double totaleIncassoCassa = 0;
                    if (s.getDataFine() == null) {
                        totaleIncassoCassa = comandaService.totaleCassa(cassa, s.getDataInizio());
                    } else {
                        totaleIncassoCassa = comandaService.totaleCassa(cassa, s.getDataInizio(), s.getDataFine());
                    }
                    sn = new PdfPCell(new Phrase(cassa.getNome() + " " + totaleIncassoCassa));
                    tableTotaleSessione.addCell(sn);


                    totaleImporto += totaleIncassoCassa;
                }


            /*    totaleSessione.setTotale(totaleImporto);
                totale.add(totaleSessione);
*/

                sn = new PdfPCell(new Phrase("Totale Sessione: " + totaleImporto, boldFontSessione));
                sn.setBorderColor(BaseColor.BLACK);
                tableTotaleSessione.addCell(sn);
                tableTotaleSessione.setSpacingBefore(10f);
                document.add(tableTotaleSessione);


            }

            document.newPage();
            p = new Paragraph(new Phrase(lineSpacing, "Prodotti Venduti",
                    boldFont));
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);

            //Sessione Totale sagra prodotti
            p = new Paragraph(new Phrase(lineSpacing - 5, "Totale sagra",
                    boldFontTotale));
            document.add(p);

            Collection<IProdottoTotale> listaProdottiTutti = prodottoService.totaleProdotti();


            PdfPTable tableTotaleProdottiSagra = new PdfPTable(1);
            sn = null;

            for (IProdottoTotale prodotto : listaProdottiTutti) {
                //  System.out.println("prodotto "+prodotto);

                int totaleNumeroProdotti = prodotto.getTotale();
                //TotaleProdotto totaleProdotto = new TotaleProdotto();
                //totaleProdotto.setNome(prodotto.getNome());
                //totaleProdotto.setTotaleProdotto(totaleNumeroProdotti);
                //totaleSagra.getTotaleProdotto().add(totaleProdotto);

                sn = new PdfPCell(new Phrase(totaleNumeroProdotti + " " + prodotto.getNome()));
                tableTotaleProdottiSagra.addCell(sn);

                //  System.out.println("Totale prodotto "+totaleProdotto);
            }

            tableTotaleProdottiSagra.setSpacingBefore(10f);
            document.add((tableTotaleProdottiSagra));






















                /*
        calcolo il toale per ogni sessione e per ogni cassa
         */
           listaSessioni = sessioneService.listaSessioni();
            listaProdottiTutti=null;
            for (Sessione s : listaSessioni) {
                PdfPTable tableTotaleProdottiSessione = new PdfPTable(1);
                sn = null;

                String nomeSessione=null;
                if(s.getDataFine()!=null){
                    nomeSessione=(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+"  -  "+
                            s.getDataFine().getYear()+"/"+s.getDataFine().getMonthValue()+"/"+ s.getDataFine().getDayOfMonth() +"  "+s.getDataFine().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())
                    );
                   listaProdottiTutti=prodottoService.totaleCassa(s.getDataInizio(),s.getDataFine());
                }
                else{
                    nomeSessione=(s.getDataInizio().getYear()+"/"+s.getDataInizio().getMonthValue()+"/"+ s.getDataInizio().getDayOfMonth()+ "  "+s.getDataInizio().getHour()+"."+String.format("%02d",s.getDataInizio().getMinute())+ "null"
                    );
                   listaProdottiTutti=prodottoService.totaleCassa(s.getDataInizio());
                }


                p = new Paragraph(new Phrase(lineSpacing + 25, nomeSessione,
                        boldFontTotale));
                document.add(p);

                int tavoli=0,tavoliOmaggio=0,asporto=0,asportoOmaggio=0;

                Set<Comanda> comandeSessione = comandaService.comandeSessione(s);

            /*
            scorro tutte le comande per decidere come mettere i totali dei coperti
             */

                for(Comanda c:comandeSessione){
                    if(c.getTavolo()){
                        tavoli+=c.getCoperti();
                        tavoliOmaggio+=c.getOmaggio();
                    }else{
                        asporto+=c.getCoperti();
                        asportoOmaggio+=c.getOmaggio();
                    }
                }

                String totaliCoperti="Totale coperti serata: "+(tavoli+tavoliOmaggio+asporto+asportoOmaggio)+" \nTavoli: "+tavoli+" + Tavoli Omaggio "+tavoliOmaggio+" + Asporto: "+asporto+" + Asporto Omaggio "+asportoOmaggio;



                for (IProdottoTotale prodotto : listaProdottiTutti) {



                    int totaleNumeroProdotti = prodotto.getTotale();/*
                    TotaleProdotto totaleProdotto = new TotaleProdotto();
                    totaleProdotto.setNome(prodotto.getNome());
                    totaleProdotto.setTotaleProdotto(totaleNumeroProdotti);*/
                    //totaleSessione.getTotaleProdotto().add(totaleProdotto);
                    sn = new PdfPCell(new Phrase(totaleNumeroProdotti + " " + prodotto.getNome()));
                    tableTotaleProdottiSessione.addCell(sn);
                }
                tableTotaleProdottiSessione.setSpacingBefore(10f);
document.add(tableTotaleProdottiSessione);
                p = new Paragraph(new Phrase( totaliCoperti));
                document.add(p);

               //
                // totale.add(totaleSessione);
            }























            document.close();
        } catch (DocumentException e) {
            logger.error(e.toString());
        }
        return new ByteArrayInputStream(out.toByteArray());

    }


}

