package it.alessiorossato.sagriamo.pdf;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.DettaglioComanda;
import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.ProdottoDAO;
import it.alessiorossato.sagriamo.services.ProdottoService;
import it.alessiorossato.sagriamo.util.ComandaUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PDFGeneratorA5 {
    private static Logger logger = LoggerFactory.getLogger(PDFGenerator.class);

    @Autowired
    ComandaDAO comandaDAO;

    @Autowired
    ProdottoDAO prodottoDAO;


    public ByteArrayInputStream pdfCliente(Comanda comanda) {
        System.out.println("Genero pdf A5");
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            PdfWriter.getInstance(document, out);
            document.setPageSize(PageSize.A5);//aggiunto io
            document.setMargins(10, 10, 10, 10);
            document.open();


            // Add Text to PDF file ->
            Font font = FontFactory.getFont(null, 30, BaseColor.BLACK);
            String minuti = comanda.getDataOra().getMinute() + "";
            if (minuti.length() == 1) {
                minuti = "0" + minuti;
            }
            String testataDati = "ID: " + comanda.getId() + " " + comanda.getCassa().getNome() + " Ore " + comanda.getDataOra().getHour() + ":" + minuti + " del " + comanda.getDataOra().getDayOfMonth() + "/" + comanda.getDataOra().getMonth().getValue() + "/" + comanda.getDataOra().getYear();


            Image img = Image.getInstance("src\\main\\java\\it\\alessiorossato\\sagriamo\\pdf\\noi.jpg");
            img.scaleAbsolute(10, 10);

            //metto il logo e l'intestazione
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell immagine = new PdfPCell(img, true);
            immagine.setBorder(Rectangle.NO_BORDER);
            //     immagine.setPaddingLeft(4);
            //  immagine.setVerticalAlignment(Element.ALIGN_MIDDLE);
            immagine.setHorizontalAlignment(Element.ALIGN_LEFT);
            immagine.setFixedHeight((float) 50.0);
            table.addCell(immagine);


            float fntSize, lineSpacing;
            fntSize = 10f;
            lineSpacing = 10f;
            Paragraph p = new Paragraph(new Phrase(lineSpacing, testataDati,
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, fntSize)));

            //   Phrase  p= new Phrase(testataDati);
            // p.setFont(font);
            PdfPCell idCell = new PdfPCell(p);
            idCell.setBorder(Rectangle.NO_BORDER);
            //   idCell.setPaddingLeft(4);
            //   idCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            idCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(idCell);

            document.add(table);
            document.add(Chunk.NEWLINE);


            //metto il progressivo
            PdfPTable tableProgressivo = new PdfPTable(2);
            tableProgressivo.setWidthPercentage(100);
            tableProgressivo.setWidths(new float[]{1, 1});
            PdfPCell cellaVuota = new PdfPCell(new Phrase("Progressivo"));
            cellaVuota.setBorder(Rectangle.NO_BORDER);
            tableProgressivo.addCell(cellaVuota);
            PdfPCell cellaProgressivo = new PdfPCell(new Phrase("" + comanda.getProgressivo()));
            cellaProgressivo.setHorizontalAlignment(Element.ALIGN_CENTER);
           /* cellaProgressivo.setBorderColor(BaseColor.BLACK);
            cellaProgressivo.setBorder(Rectangle.BOX);
            cellaProgressivo.setBorder(6);*/
            cellaProgressivo.setBorderWidth(2);
            tableProgressivo.addCell(cellaProgressivo);
            document.add(tableProgressivo);
            document.add(Chunk.NEWLINE);


            //metto la intestazione  tavoli coperti

            PdfPTable tableHeader = new PdfPTable(2);
            tableHeader.setWidthPercentage(100);
            tableHeader.setWidths(new float[]{1, 1});

            String tipo = "";
            String coperti = "";
            if (comanda.isTavolo()) {
                tipo = "Tavolo";
                coperti = "Coperti: " + (comanda.getCoperti() + comanda.getOmaggio());
            } else {
                tipo = "Asporto";
            }
            PdfPCell tipoComanda = new PdfPCell(new Phrase("Tipo: " + tipo));
            tipoComanda.setBorder(Rectangle.NO_BORDER);
            tipoComanda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tableHeader.addCell(tipoComanda);

            PdfPCell numeroCoperti = new PdfPCell(new Phrase(coperti));
            numeroCoperti.setBorder(Rectangle.NO_BORDER);
            numeroCoperti.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tableHeader.addCell(numeroCoperti);
            tableHeader.setSpacingBefore(20);

            document.add(tableHeader);
            document.add(Chunk.NEWLINE);


            //metto la tabella con i dati


            PdfPTable tableData = new PdfPTable(3);
            tableData.setWidthPercentage(100);
            tableData.setWidths(new float[]{1, 3, 3});
            PdfPCell sn = new PdfPCell(new Phrase("Quantità"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //   sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);
            sn = new PdfPCell(new Phrase("Prodotto"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //   sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);
            sn = new PdfPCell(new Phrase("Prezzo"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);


            for (DettaglioComanda t : comanda.getDettaglioComandas()) {
                String quantita = t.getQta() + "(+" + t.getQtaO() + ")";

                String nome = t.getProdotto().getNome();

                String prezzo = t.getPrezzo() + "X" + t.getQta() + "=" + t.getPrezzoTotale();

                PdfPCell cella = new PdfPCell(new Phrase(quantita));
                cella.setBorder(Rectangle.BOTTOM);

                tableData.addCell(cella);

                cella = new PdfPCell(new Phrase(nome));
                cella.setBorder(Rectangle.BOTTOM);
                cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                tableData.addCell(cella);

                cella = new PdfPCell(new Phrase(prezzo));
                cella.setBorder(Rectangle.BOTTOM);
                cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                tableData.addCell(cella);

            }


            PdfPCell cella = new PdfPCell();
            cella.setBorderWidth(2);
            cella.setBorder(Rectangle.TOP);
            cella.setBorderColor(BaseColor.BLACK);
            tableData.addCell(cella);
            tableData.addCell(cella);
            tableData.addCell(cella);
            tableData.setSpacingBefore(10);

            document.add(tableData);
            document.add(Chunk.NEWLINE);


            //metto il totale della comanda
            PdfPTable tableTotale = new PdfPTable(2);
            tableTotale.setWidthPercentage(100);
            tableTotale.setWidths(new float[]{1, 1});

            Font boldFont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);

            PdfPCell testoTotale = new PdfPCell(new Phrase("Totale:", boldFont));
            testoTotale.setBorder(Rectangle.NO_BORDER);
            testoTotale.setHorizontalAlignment(Element.ALIGN_LEFT);
            tableTotale.addCell(testoTotale);

            PdfPCell testoTotaleNumero = new PdfPCell(new Phrase("€ " + comanda.getTotale(), boldFont));
            testoTotaleNumero.setBorder(Rectangle.NO_BORDER);
            testoTotaleNumero.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableTotale.addCell(testoTotaleNumero);


            document.add(tableTotale);
            document.add(Chunk.NEWLINE);


            //meto le note


            if (comanda.getNote() != null) {
                PdfPTable tableNote = new PdfPTable(1);
                tableNote.setSpacingBefore(30);
                tableNote.setWidthPercentage(100);
                PdfPCell cellaNote = new PdfPCell(new Phrase("Note: " + comanda.getNote()));
                cellaNote.setBorder(Rectangle.NO_BORDER);
                tableNote.addCell(cellaNote);
                document.add(tableNote);
                document.add(Chunk.NEWLINE);

            }


            document.close();
        } catch (DocumentException e) {
            logger.error(e.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    }


    public ByteArrayInputStream pdfReparto(Comanda comanda,Set<Long> idReparti) {
        System.out.println("Genero pdf A5 Cucina");
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
   //     Font font = FontFactory.getFont(null, 30, BaseColor.BLACK);

        try {

            PdfWriter.getInstance(document, out);
            document.setPageSize(PageSize.A5);//aggiunto io
            document.setMargins(10, 10, 10, 10);
            document.open();

            String tipo = "";
            String coperti = "";
            if (comanda.isTavolo()) {
                tipo = "Tavolo";
                coperti = "Coperti: " + (comanda.getCoperti() + comanda.getOmaggio());
            } else {
                tipo = "Asporto";
            }

            Phrase tipoC = new Phrase("Tipo: " + tipo);
            PdfPCell tipoComanda = new PdfPCell();
            tipoComanda.setBorder(Rectangle.NO_BORDER);
            tipoComanda.setHorizontalAlignment(Element.ALIGN_LEFT);
            tipoComanda.addElement(tipoC);

            Phrase copertiC = new Phrase(coperti);
            tipoComanda.addElement(copertiC);


            // Add Text to PDF file ->
            Font font = FontFactory.getFont(null, 10, BaseColor.BLACK);
            Font font15 = FontFactory.getFont(null, 15, BaseColor.BLACK);
            Font font20 = FontFactory.getFont(null, 20, BaseColor.BLACK);
            String minuti = comanda.getDataOra().getMinute() + "";
            if (minuti.length() == 1) {
                minuti = "0" + minuti;
            }
            String testataDati = "ID: " + comanda.getId() + " " + comanda.getCassa().getNome() + " Ore " + comanda.getDataOra().getHour() + ":" + minuti + " del " + comanda.getDataOra().getDayOfMonth() + "/" + comanda.getDataOra().getMonth().getValue() + "/" + comanda.getDataOra().getYear();


            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);

            table.addCell(tipoComanda);
            PdfPCell info = new PdfPCell();
            info.addElement(new Phrase(testataDati, font));
            info.addElement(new Phrase("Progressivo:"));
            info.setBorder(Rectangle.NO_BORDER);
            table.addCell(info);

            document.add(table);



         /*   //metto il logo e l'intestazione
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell immagine = new PdfPCell(img, true);
            immagine.setBorder(Rectangle.NO_BORDER);
            //     immagine.setPaddingLeft(4);
            //  immagine.setVerticalAlignment(Element.ALIGN_MIDDLE);
            immagine.setHorizontalAlignment(Element.ALIGN_LEFT);
            immagine.setFixedHeight((float) 50.0);
            table.addCell(immagine);*/


         /*   float fntSize, lineSpacing;
            fntSize = 10f;
            lineSpacing = 10f;
            Paragraph p = new Paragraph(new Phrase(lineSpacing, testataDati,
                FontFactory.getFont(FontFactory.TIMES_ROMAN, fntSize)));

            //   Phrase  p= new Phrase(testataDati);
            // p.setFont(font);
            PdfPCell idCell = new PdfPCell(p);
            idCell.setBorder(Rectangle.NO_BORDER);
            //   idCell.setPaddingLeft(4);
            //   idCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            idCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(idCell);

            document.add(table);
            document.add(Chunk.NEWLINE);

*/
            //metto il progressivo
            PdfPTable tableProgressivo = new PdfPTable(2);
            tableProgressivo.setWidthPercentage(100);
            tableProgressivo.setWidths(new float[]{1, 1});
            PdfPCell cellaVuota = new PdfPCell();//cella vuota per lo spazio per mettere il numero del progressivo
            cellaVuota.setBorder(Rectangle.NO_BORDER);
            tableProgressivo.addCell(cellaVuota);
            PdfPCell cellaProgressivo = new PdfPCell(new Phrase("" + comanda.getProgressivo(),font20));

            cellaProgressivo.setHorizontalAlignment(Element.ALIGN_CENTER);
           /* cellaProgressivo.setBorderColor(BaseColor.BLACK);
            cellaProgressivo.setBorder(Rectangle.BOX);
            cellaProgressivo.setBorder(6);*/
            cellaProgressivo.setBorderWidth(2);
            tableProgressivo.addCell(cellaProgressivo);
            document.add(tableProgressivo);
            document.add(Chunk.NEWLINE);


            //metto la intestazione  tavoli coperti

            PdfPTable tableHeader = new PdfPTable(2);
            tableHeader.setWidthPercentage(100);
            tableHeader.setWidths(new float[]{1, 1});


            document.add(tableHeader);
            document.add(Chunk.NEWLINE);


            //metto la tabella con i dati


            PdfPTable tableData = new PdfPTable(2);
            tableData.setWidthPercentage(100);
            tableData.setWidths(new float[]{1, 3});
            PdfPCell sn = new PdfPCell(new Phrase("Quantità"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //   sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);
            sn = new PdfPCell(new Phrase("Prodotto"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //   sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);
           /* sn = new PdfPCell(new Phrase("Prezzo"));
            sn.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    sn.setRowspan(2);
            sn.setBackgroundColor(BaseColor.YELLOW);
            tableData.addCell(sn);*/

//recupero tutti i prodotti di un reparto
           // Set<DettaglioComanda> listaProdottiReparto= ComandaUtility.prodottiCategoria(comanda,idReparti);
            List<DettaglioComanda> listaProdottiReparto = prodottiReparto(comanda, idReparti);

            for (DettaglioComanda t :listaProdottiReparto) {

               // if(!t.getProdotto().getCategoria().isBar()){
                    String quantita = ""+(t.getQta()+t.getQtaO());//t.getQta() + "(+" + t.getQtaO() + ")";

                    String nome = t.getProdotto().getNome();

                    String prezzo = t.getPrezzo() + "X" + t.getQta() + "=" + t.getPrezzoTotale();

                    PdfPCell cella = new PdfPCell(new Phrase(quantita,font15));
                    cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cella.setBorder(Rectangle.BOTTOM);

                    tableData.addCell(cella);

                    cella = new PdfPCell(new Phrase(nome,font15));
                    cella.setBorder(Rectangle.BOTTOM);
                    cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableData.addCell(cella);

             /*   cella = new PdfPCell(new Phrase(prezzo));
                cella.setBorder(Rectangle.BOTTOM);
                cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                tableData.addCell(cella);*/
         //       }


            }


            PdfPCell cella = new PdfPCell();
            cella.setBorderWidth(2);
            cella.setBorder(Rectangle.TOP);
            cella.setBorderColor(BaseColor.BLACK);
            tableData.addCell(cella);
            tableData.addCell(cella);
            tableData.addCell(cella);
            tableData.setSpacingBefore(10);

            document.add(tableData);
            document.add(Chunk.NEWLINE);


            //metto il totale della comanda
         /*   PdfPTable tableTotale = new PdfPTable(2);
            tableTotale.setWidthPercentage(100);
            tableTotale.setWidths(new float[]{1, 1});

            Font boldFont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);

            PdfPCell testoTotale = new PdfPCell(new Phrase("Totale:", boldFont));
            testoTotale.setBorder(Rectangle.NO_BORDER);
            testoTotale.setHorizontalAlignment(Element.ALIGN_LEFT);
            tableTotale.addCell(testoTotale);

            PdfPCell testoTotaleNumero = new PdfPCell(new Phrase("€ " + comanda.getTotale(), boldFont));
            testoTotaleNumero.setBorder(Rectangle.NO_BORDER);
            testoTotaleNumero.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableTotale.addCell(testoTotaleNumero);


            document.add(tableTotale);*/
            document.add(Chunk.NEWLINE);


            //meto le note


            if (comanda.getNote() != null) {
                PdfPTable tableNote = new PdfPTable(1);
                tableNote.setSpacingBefore(30);
                tableNote.setWidthPercentage(100);
                PdfPCell cellaNote = new PdfPCell(new Phrase("Note: " + comanda.getNote()));
                cellaNote.setBorder(Rectangle.NO_BORDER);
                tableNote.addCell(cellaNote);
                document.add(tableNote);
                document.add(Chunk.NEWLINE);

            }


            document.close();
        } catch (DocumentException e) {
            logger.error(e.toString());
      /*  } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();*/
        }
        return new ByteArrayInputStream(out.toByteArray());
    }


    private  List<DettaglioComanda> prodottiReparto(Comanda comanda, Set<Long> idReparti){

        List<DettaglioComanda> result=new ArrayList<>();
        for(DettaglioComanda dc:comanda.getDettaglioComandas()){
            Long idProdotto=dc.getProdotto().getId();
            Optional<Prodotto> p=prodottoDAO.findById(idProdotto);
            if(p.isPresent()) {
                if (idReparti.contains(p.get().getCategoria().getReparto().getId())) {
                    dc.setProdotto(p.get());
                    result.add(dc);
                }
            }
        }
        return result;
    }

}

