package it.alessiorossato.sagriamo.bean;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class Storico {
    private Long id;

    private LocalDateTime dataOra;

    private String cassa;

    private Integer progressivo;

    private boolean cucina;

    public Storico() {
    }

    public Storico(Long id, LocalDateTime dataOra, String cassa, int progressivo) {
        this.id = id;
        this.dataOra = dataOra;
        this.cassa = cassa;
        this.progressivo = progressivo;
        this.cucina = cucina;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataOra() {
        return dataOra;
    }

    public void setDataOra(LocalDateTime dataOra) {
        this.dataOra = dataOra;
    }

    public String getCassa() {
        return cassa;
    }

    public void setCassa(String cassa) {
        this.cassa = cassa;
    }

    public Integer getProgressivo() {
        return progressivo;
    }

    public void setProgressivo(Integer progressivo) {
        this.progressivo = progressivo;
    }

    public boolean isCucina() {
        return cucina;
    }

    public void setCucina(boolean cucina) {
        this.cucina = cucina;
    }

    @Override
    public String toString() {
        return "Storico{" +
            "id=" + id +
            ", dataOra=" + dataOra +
            ", cassa='" + cassa + '\'' +
            ", progressivo=" + progressivo +
            ", cucina=" + cucina +
            '}';
    }
}
