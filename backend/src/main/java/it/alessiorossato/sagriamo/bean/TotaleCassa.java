package it.alessiorossato.sagriamo.bean;


import java.util.ArrayList;

public class TotaleCassa {


    private String nome;

    private double totaleCassa;

    public TotaleCassa() {
    }

    public TotaleCassa(String nome, double totaleCassa) {
        this.nome = nome;
        this.totaleCassa = totaleCassa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTotaleCassa() {
        return totaleCassa;
    }

    public void setTotaleCassa(double totaleCassa) {
        this.totaleCassa = totaleCassa;
    }
}
