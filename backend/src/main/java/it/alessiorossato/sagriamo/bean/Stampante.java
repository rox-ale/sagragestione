package it.alessiorossato.sagriamo.bean;

import it.alessiorossato.sagriamo.model.Reparto;

import java.util.HashSet;
import java.util.Set;

public class Stampante {

    private Long id;

    private String nome;

    private String ip;

    private String formato;

    private String nomeStampantePc;

    private Set<Cassa> cassas = new HashSet<>();
    private Reparto reparto;

    public Reparto getReparto() {
        return reparto;
    }

    public void setReparto(Reparto reparto) {
        this.reparto = reparto;
    }

    public Stampante() {
    }

    public Stampante(Long id, String nome, String ip, String formato, String nomeStampantePc, Set<Cassa> cassas) {
        this.id = id;
        this.nome = nome;
        this.ip = ip;
        this.formato = formato;
        this.nomeStampantePc = nomeStampantePc;
        this.cassas = cassas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNomeStampantePc() {
        return nomeStampantePc;
    }

    public void setNomeStampantePc(String nomeStampantePc) {
        this.nomeStampantePc = nomeStampantePc;
    }

    public Set<Cassa> getCassas() {
        return cassas;
    }

    public void setCassas(Set<Cassa> cassas) {
        this.cassas = cassas;
    }
}
