package it.alessiorossato.sagriamo.bean;

import java.util.HashSet;
import java.util.Set;

public class Prodotto  {


    private Long id;


    private String nome;


    private Double prezzo;


    private Boolean abilitato;


    private Categoria categoria;


   private int qta;
private int    qtaO;

    private Set<Ricetta> ricetta=new HashSet<>();

    public Prodotto() {
        qta=0;
        qtaO=0;
    }

    public Prodotto(Long id, String nome, Double prezzo, Boolean abilitato, Categoria categoria) {
        this.id = id;
        this.nome = nome;
        this.prezzo = prezzo;
        this.abilitato = abilitato;
        this.categoria = categoria;
        qta=0;
        qtaO=0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public Boolean getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    public int getQtaO() {
        return qtaO;
    }

    public void setQtaO(int qtaO) {
        this.qtaO = qtaO;
    }

    public Set<Ricetta> getRicetta() {
        return ricetta;
    }

    public void setRicetta(Set<Ricetta> ricetta) {
        this.ricetta = ricetta;
    }

    @Override
    public String toString() {
        return "Prodotto{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", prezzo=" + prezzo +
                ", abilitato=" + abilitato +
                ", qta=" + qta +
                ", qtaO=" + qtaO +
                ", ricetta=" + ricetta +
                '}';
    }
}
