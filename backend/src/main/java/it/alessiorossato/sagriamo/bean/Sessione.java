package it.alessiorossato.sagriamo.bean;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A Cassa.
 */

public class Sessione   {


    private Long id;


    private LocalDateTime dataInizio;


    private LocalDateTime dataFine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(LocalDateTime dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDateTime getDataFine() {
        return dataFine;
    }

    public void setDataFine(LocalDateTime dataFine) {
        this.dataFine = dataFine;
    }

    @Override
    public String toString() {
        return "Sessione{" +
                "id=" + id +
                ", dataInizio=" + dataInizio +
                ", dataFine=" + dataFine +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sessione sessione = (Sessione) o;
        return Objects.equals(id, sessione.id) &&
                Objects.equals(dataInizio, sessione.dataInizio) &&
                Objects.equals(dataFine, sessione.dataFine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataInizio, dataFine);
    }
}
