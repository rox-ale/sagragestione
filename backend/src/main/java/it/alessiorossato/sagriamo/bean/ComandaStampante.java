package it.alessiorossato.sagriamo.bean;

public class ComandaStampante {
   private Comanda comanda;

    private Stampante stampante;

    public ComandaStampante() {
    }

    public ComandaStampante(Comanda comanda, Stampante stampante) {
        this.comanda = comanda;
        this.stampante = stampante;
    }

    public Comanda getComanda() {
        return comanda;
    }

    public void setComanda(Comanda comanda) {
        this.comanda = comanda;
    }

    public Stampante getStampante() {
        return stampante;
    }

    public void setStampante(Stampante stampante) {
        this.stampante = stampante;
    }
}
