package it.alessiorossato.sagriamo.bean;



public class Cassa {

    private Long id;


    private String nome;


    private Boolean abilitato;


    private Boolean bar;


    private Boolean asporto;

    private Stampante stampante;

    public Cassa() {
    }

    public Stampante getStampante() {
        return stampante;
    }

    public void setStampante(Stampante stampante) {
        this.stampante = stampante;
    }

    public Cassa(Long id, String nome, Boolean abilitato, Boolean bar, Boolean asporto, Stampante stampante) {
        this.id = id;
        this.nome = nome;
        this.abilitato = abilitato;
        this.bar = bar;
        this.asporto = asporto;
        this.stampante = stampante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Boolean getBar() {
        return bar;
    }

    public void setBar(Boolean bar) {
        this.bar = bar;
    }

    public Boolean getAsporto() {
        return asporto;
    }

    public void setAsporto(Boolean asporto) {
        this.asporto = asporto;
    }

    @Override
    public String toString() {
        return "Cassa{" +
            "id=" + id +
            ", nome='" + nome + '\'' +
            ", abilitato=" + abilitato +
            ", bar=" + bar +
            ", asporto=" + asporto +
            '}';
    }
}
