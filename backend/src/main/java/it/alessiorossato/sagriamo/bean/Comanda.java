package it.alessiorossato.sagriamo.bean;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class Comanda {

    private Long id;

    private Integer progressivo;

    private LocalDateTime dataOra;

    private Boolean tavolo;

    private Double totale;

    private Double versato;

    private Double resto;

    private Integer coperti;

    private Integer omaggio;

    private Cassa cassa;

    private String note;

    private LocalDateTime consegnato;

    private LocalDateTime preparazione;

    private LocalDateTime ritirato;

    private Set<DettaglioComanda> dettaglioComandas = new HashSet<>();

    public Comanda() {
    }

    public Comanda(Long id, Integer progressivo, LocalDateTime dataOra, Boolean tavolo, Double totale, Double versato, Double resto, Integer coperti, Integer omaggio, Cassa cassa, String note, LocalDateTime consegnato, LocalDateTime preparazione, LocalDateTime ritirato, Set<DettaglioComanda> dettaglioComandas) {
        this.id = id;
        this.progressivo = progressivo;
        this.dataOra = dataOra;
        this.tavolo = tavolo;
        this.totale = totale;
        this.versato = versato;
        this.resto = resto;
        this.coperti = coperti;
        this.omaggio = omaggio;
        this.cassa = cassa;
        this.note = note;
        this.consegnato = consegnato;
        this.preparazione = preparazione;
        this.ritirato = ritirato;
        this.dettaglioComandas = dettaglioComandas;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProgressivo() {
        return progressivo;
    }

    public void setProgressivo(Integer progressivo) {
        this.progressivo = progressivo;
    }

    public LocalDateTime getDataOra() {
        return dataOra;
    }

    public void setDataOra(LocalDateTime dataOra) {
        this.dataOra = dataOra;
    }

    public Boolean getTavolo() {
        return tavolo;
    }

    public void setTavolo(Boolean tavolo) {
        this.tavolo = tavolo;
    }

    public Double getTotale() {
        return totale;
    }

    public void setTotale(Double totale) {
        this.totale = totale;
    }

    public Double getVersato() {
        return versato;
    }

    public void setVersato(Double versato) {
        this.versato = versato;
    }

    public Double getResto() {
        return resto;
    }

    public void setResto(Double resto) {
        this.resto = resto;
    }

    public Integer getCoperti() {
        return coperti;
    }

    public void setCoperti(Integer coperti) {
        this.coperti = coperti;
    }

    public Integer getOmaggio() {
        return omaggio;
    }

    public void setOmaggio(Integer omaggio) {
        this.omaggio = omaggio;
    }

    public Cassa getCassa() {
        return cassa;
    }

    public void setCassa(Cassa cassa) {
        this.cassa = cassa;
    }

    public Set<DettaglioComanda> getDettaglioComandas() {
        return dettaglioComandas;
    }

    public void setDettaglioComandas(Set<DettaglioComanda> dettaglioComandas) {
        this.dettaglioComandas = dettaglioComandas;
    }

    public LocalDateTime getConsegnato() {
        return consegnato;
    }

    public void setConsegnato(LocalDateTime consegnato) {
        this.consegnato = consegnato;
    }

    public LocalDateTime getPreparazione() {
        return preparazione;
    }

    public void setPreparazione(LocalDateTime preparazione) {
        this.preparazione = preparazione;
    }

    public LocalDateTime getRitirato() {
        return ritirato;
    }

    public void setRitirato(LocalDateTime ritirato) {
        this.ritirato = ritirato;
    }

    @Override
    public String toString() {
        return "Comanda{" +
            "id=" + id +
            ", progressivo=" + progressivo +
            ", dataOra=" + dataOra +
            ", tavolo=" + tavolo +
            ", totale=" + totale +
            ", versato=" + versato +
            ", resto=" + resto +
            ", coperti=" + coperti +
            ", omaggio=" + omaggio +
            ", cassa=" + cassa +
            ", dettaglioComandas=" + dettaglioComandas +
            '}';
    }
}
