package it.alessiorossato.sagriamo.bean;


import java.util.ArrayList;

public class TotaleSessioneProdotto {

/*
nome della sessione
 */
    private String nome;

    private ArrayList<TotaleProdotto> totaleProdotto;

    private String totali;
    public TotaleSessioneProdotto() {
        totaleProdotto =new ArrayList<>();
    }

    public TotaleSessioneProdotto(String nome, ArrayList<TotaleProdotto> totaleCasse,String totali) {
        this.nome = nome;
        this.totaleProdotto = totaleCasse;
        this.totali=totali;

    }



    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<TotaleProdotto> getTotaleProdotto() {
        return totaleProdotto;
    }

    public void setTotaleProdotto(ArrayList<TotaleProdotto> totaleProdotto) {
        this.totaleProdotto = totaleProdotto;
    }

    public String getTotali() {
        return totali;
    }

    public void setTotali(String totali) {
        this.totali = totali;
    }
}
