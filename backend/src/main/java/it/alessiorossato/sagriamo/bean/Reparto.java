package it.alessiorossato.sagriamo.bean;



import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Reparto.
 */

public class Reparto   {


    private Long id;


    private String nome;


    private Boolean abilitato;


    private Stampante stampante;


    private Set<Categoria> categoria = new HashSet<>();

    public Reparto(String nome, Boolean abilitato, Stampante stampante, Set<Categoria> categoria) {
        this.nome = nome;
        this.abilitato = abilitato;
        this.stampante = stampante;
        this.categoria = categoria;
    }

    public Reparto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Stampante getStampante() {
        return stampante;
    }

    public void setStampante(Stampante stampante) {
        this.stampante = stampante;
    }

    public Set<Categoria> getCategoria() {
        return categoria;
    }

    public void setCategoria(Set<Categoria> categoria) {
        this.categoria = categoria;
    }
}

