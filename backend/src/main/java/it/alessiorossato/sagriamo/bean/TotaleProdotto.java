package it.alessiorossato.sagriamo.bean;


public class TotaleProdotto {


    private String nome;

    private int totaleProdotto;

    public TotaleProdotto() {
    }

    public TotaleProdotto(String nome, int totaleCassa) {
        this.nome = nome;
        this.totaleProdotto = totaleCassa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTotaleProdotto() {
        return totaleProdotto;
    }


    public void setTotaleProdotto(int totaleCassa) {
        this.totaleProdotto = totaleCassa;
    }

    @Override
    public String toString() {
        return "TotaleProdotto{" +
                "nome='" + nome + '\'' +
                ", totaleProdotto=" + totaleProdotto +
                '}';
    }
}
