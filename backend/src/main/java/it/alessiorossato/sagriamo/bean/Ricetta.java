package it.alessiorossato.sagriamo.bean;




/**
 * A DettaglioComanda.
 */

public class Ricetta   {



   private Long id;

    private Integer qta;

    private Prodotto prodotto;

    private Ingrediente ingrediente;


    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Ricetta{" +
                "id=" + id +
                ", qta=" + qta +
                '}';
    }
}
