package it.alessiorossato.sagriamo.bean;


import java.util.ArrayList;

public class TotaleSessioneCassa {


    private String nome;

    private ArrayList<TotaleCassa> totaleCasse;

    private double totale;
    public TotaleSessioneCassa() {
        totaleCasse=new ArrayList<>();
    }

    public TotaleSessioneCassa(String nome, ArrayList<TotaleCassa> totaleCasse, double totale) {
        this.nome = nome;
        this.totaleCasse = totaleCasse;
        this.totale = totale;
    }



    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<TotaleCassa> getTotaleCasse() {
        return totaleCasse;
    }

    public void setTotaleCasse(ArrayList<TotaleCassa> totaleCasse) {
        this.totaleCasse = totaleCasse;
    }

    public double getTotale() {
        return totale;
    }

    public void setTotale(double totale) {
        this.totale = totale;
    }
}
