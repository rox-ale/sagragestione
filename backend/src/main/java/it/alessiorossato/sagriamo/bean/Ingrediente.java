package it.alessiorossato.sagriamo.bean;



import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;



public class Ingrediente implements Serializable {


    private Long id;


    private String nome;


    private Boolean griglia;

    private Set< Ricetta> ricetta=new HashSet<>();

    public Ingrediente() {
    }

    public Ingrediente(String nome, Boolean griglia, Set<Ricetta> ricetta) {
        this.nome = nome;
        this.griglia = griglia;
        this.ricetta = ricetta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getGriglia() {
        return griglia;
    }

    public void setGriglia(Boolean griglia) {
        this.griglia = griglia;
    }

    public Set<Ricetta> getRicetta() {
        return ricetta;
    }

    public void setRicetta(Set<Ricetta> ricetta) {
        this.ricetta = ricetta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingrediente that = (Ingrediente) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nome, that.nome) &&
                Objects.equals(griglia, that.griglia) &&
                Objects.equals(ricetta, that.ricetta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, griglia, ricetta);
    }


    public String toString() {
        return "Ingrediente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", griglia=" + griglia +
                ", ricetta=" + ricetta +
                '}';
    }
}
