package it.alessiorossato.sagriamo.bean;



import java.util.HashSet;
import java.util.Set;

public class Categoria {


    private Long id;


    private String nome;





    private Boolean abilitato;


    private Boolean bar;


    private Set<Prodotto> prodottos = new HashSet<>();
    private Reparto reparto;

    public Categoria() {
    }

    public Categoria(Long id, String nome, Boolean abilitato, Boolean bar, Set<Prodotto> prodottos, Reparto reparto) {
        this.id = id;
        this.nome = nome;
        this.abilitato = abilitato;
        this.bar = bar;
        this.prodottos = prodottos;
        this.reparto = reparto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Boolean getBar() {
        return bar;
    }

    public void setBar(Boolean bar) {
        this.bar = bar;
    }

    public Set<Prodotto> getProdottos() {
        return prodottos;
    }

    public void setProdottos(Set<Prodotto> prodottos) {
        this.prodottos = prodottos;
    }

    public Reparto getReparto() {
        return reparto;
    }

    public void setReparto(Reparto reparto) {
        this.reparto = reparto;
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", abilitato=" + abilitato +
                ", bar=" + bar +
                ", prodottos=" + prodottos +
                ", reparto=" + reparto +
                '}';
    }
}
