package it.alessiorossato.sagriamo.bean;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class DettaglioComanda {

    private Long id;


    private Integer qta;


    private Integer qtaO;


    private LocalDateTime time;


    private Double prezzo;


    private Double prezzoTotale;


    private Prodotto prodotto;


    private Comanda comanda;

    public DettaglioComanda() {
    }

    public DettaglioComanda(Long id, Integer qta, Integer qtaO, LocalDateTime time, Double prezzo, Double prezzoTotale, Prodotto prodotto, Comanda comanda) {
        this.id = id;
        this.qta = qta;
        this.qtaO = qtaO;
        this.time = time;
        this.prezzo = prezzo;
        this.prezzoTotale = prezzoTotale;
        this.prodotto = prodotto;
        this.comanda = comanda;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    public Integer getQtaO() {
        return qtaO;
    }

    public void setQtaO(Integer qtaO) {
        this.qtaO = qtaO;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public Double getPrezzoTotale() {
        return prezzoTotale;
    }

    public void setPrezzoTotale(Double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public Comanda getComanda() {
        return comanda;
    }

    public void setComanda(Comanda comanda) {
        this.comanda = comanda;
    }

    @Override
    public String toString() {
        return "DettaglioComanda{" +
            "id=" + id +
            ", qta=" + qta +
            ", qtaO=" + qtaO +
            ", time=" + time +
            ", prezzo=" + prezzo +
            ", prezzoTotale=" + prezzoTotale +
            ", prodotto=" + prodotto +
            ", comanda=" + comanda +
            '}';
    }
}
