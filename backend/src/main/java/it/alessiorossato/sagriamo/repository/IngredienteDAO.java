package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Ingrediente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IngredienteDAO extends JpaRepository<Ingrediente, Long> {


}
