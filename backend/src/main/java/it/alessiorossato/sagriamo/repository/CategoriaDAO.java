package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CategoriaDAO extends JpaRepository<Categoria, Long> {
    @Query(value = "SELECT * FROM Categoria WHERE abilitato=true ", nativeQuery = true)
    public Set<Categoria> categorieAttive();

    @Query(value = "SELECT * FROM Categoria WHERE abilitato=true and bar=:bar ", nativeQuery = true)
    public Set<Categoria> categorieAttive(@Param("bar")boolean bar);

}
