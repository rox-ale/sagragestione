package it.alessiorossato.sagriamo.repository.query;

public class ProdottoTotale implements IProdottoTotale {
    Integer id;
    String nome;
    Integer totale;

    public ProdottoTotale(Integer id, String nome, Integer totale)  {
        this.id = id;
        this.nome = nome;
        this.totale = totale;
    }

    public ProdottoTotale() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getTotale() {
        return totale;
    }

    public void setTotale(Integer totale) {
        this.totale = totale;
    }
}
