package it.alessiorossato.sagriamo.repository;

import java.util.Optional;

import it.alessiorossato.sagriamo.model.Role;
import it.alessiorossato.sagriamo.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
