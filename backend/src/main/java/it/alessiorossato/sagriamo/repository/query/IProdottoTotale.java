package it.alessiorossato.sagriamo.repository.query;

public interface IProdottoTotale {
    public Integer getId() ;

    public String getNome();

    public Integer getTotale();

}
