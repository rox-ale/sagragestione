package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.DettaglioComanda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Repository
public interface CassaDAO extends JpaRepository<Cassa, Long> {
    @Query(value = "SELECT * FROM Cassa WHERE abilitato=true ", nativeQuery = true)
    public Set<Cassa> cassaAttive();

}
