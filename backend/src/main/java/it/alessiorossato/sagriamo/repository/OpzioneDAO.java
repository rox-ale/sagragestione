package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Ingrediente;
import it.alessiorossato.sagriamo.model.Opzione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface OpzioneDAO extends JpaRepository<Opzione, Long> {
    @Query(value = "FROM Opzione as o    WHERE o.nome='progressivo'  ",nativeQuery = false)
    public Opzione opzioneProgressivo();



}
