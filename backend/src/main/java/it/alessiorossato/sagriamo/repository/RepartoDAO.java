package it.alessiorossato.sagriamo.repository;


import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.Reparto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface RepartoDAO extends JpaRepository<Reparto, Long> {

}