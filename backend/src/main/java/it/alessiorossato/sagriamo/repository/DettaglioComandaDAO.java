package it.alessiorossato.sagriamo.repository;


import it.alessiorossato.sagriamo.model.DettaglioComanda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


@Repository
public interface DettaglioComandaDAO extends JpaRepository<DettaglioComanda, Long> {
    //@Query(value = "SELECT * FROM Stampante WHERE abilitato=true ", nativeQuery = true)
    @Modifying
    @Transactional
    @Query(value = "delete from dettaglio_comanda where comanda_id= :id ", nativeQuery = true)
    public void cancellaDettaglioComanda(@Param("id") Long id);


    @Query(value = "SELECT * FROM dettaglio_comanda as p    WHERE  p.comanda_id = :comanda", nativeQuery = true)
    public Set<DettaglioComanda> prodottiComanda(@Param("comanda") Long comanda);


}
