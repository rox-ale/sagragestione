package it.alessiorossato.sagriamo.repository;


import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Comanda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface ComandaDAO extends JpaRepository<Comanda, Long> {
    //@Query(value = "SELECT * FROM Stampante WHERE abilitato=true ", nativeQuery = true)
    @Query(value = "SELECT  c.id,c.data_ora,ca.nome,c.progressivo  FROM Comanda c join Cassa ca on ca.id= c.cassa_id ", nativeQuery = true)
     public List<Object[]> listaComande();

    @Modifying
    @Transactional
    @Query(value = "delete from comanda where id= :id ", nativeQuery = true)
    public void cancellaComanda(@Param("id") Long id);



    @Query(value = "SELECT * FROM Comanda WHERE cassa_id = :id ", nativeQuery = true)
    public Set<Comanda> listaComandeCassa(@Param("id") Long id);


    @Query(value = "SELECT * FROM Comanda WHERE (consegnato<'2019-05-06' or preparazione<'2019-05-06' or ritirato<'2019-05-06') ", nativeQuery = true)
    public Set<Comanda>     listaComandeDaFare();




    @Modifying
    @Transactional
    @Query(value = "update comanda set preparazione = :localDate where id= :id", nativeQuery = true)
    public void presaInCarico(@Param("id") Long id,@Param("localDate") LocalDate localDate);


    @Query(value = "SELECT * FROM Comanda WHERE cassa_id = :id and data_ora>= :inizio and (data_ora<=:fine or data_ora=:fine)", nativeQuery = true)
    public Set<Comanda> listaComandeCassa(@Param("id") Long id, @Param("inizio") LocalDateTime inizio, @Param("fine") LocalDateTime fine);

    @Query(value = "SELECT * FROM Comanda WHERE cassa_id = :id and data_ora>= :inizio", nativeQuery = true)
    public Set<Comanda> listaComandeCassaDataFineNull(@Param("id") Long id, @Param("inizio") LocalDateTime inizio);


    @Query(value = "SELECT * FROM Comanda WHERE   data_ora>= :inizio and (data_ora<=:fine or data_ora=:fine)", nativeQuery = true)
    public Set<Comanda> listaComandeSessione(  @Param("inizio") LocalDateTime inizio, @Param("fine") LocalDateTime fine);

    @Query(value = "SELECT * FROM Comanda WHERE   data_ora>= :inizio", nativeQuery = true)
    public Set<Comanda> listaComandeSessioneDataFineNull(  @Param("inizio") LocalDateTime inizio);


}