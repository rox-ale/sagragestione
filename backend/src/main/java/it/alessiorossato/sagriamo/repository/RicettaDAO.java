package it.alessiorossato.sagriamo.repository;


import it.alessiorossato.sagriamo.model.Ricetta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RicettaDAO extends JpaRepository<Ricetta, Long> {
    @Query(value = "SELECT * FROM Ricetta as r    WHERE r.prodotto_id = :idProdotto", nativeQuery = true)
    public Set<Ricetta> ricettaProdotto(@Param("idProdotto") Long idProdotto);
}
