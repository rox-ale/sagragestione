package it.alessiorossato.sagriamo.repository;


import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.repository.query.IProdottoTotale;
import it.alessiorossato.sagriamo.repository.query.ProdottoTotale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface ProdottoDAO extends JpaRepository<Prodotto, Long> {
    @Query(value = "SELECT * FROM Prodotto as p    WHERE p.abilitato=true  and p.categoria_id = :categoria", nativeQuery = true)
    public Set<Prodotto> prodottiAttiviCategoria(@Param("categoria") Long categoria);

    @Query(value = "SELECT p.id as id  ,p.nome as nome ,COALESCE(SUM(dc.qta+dc.qta_o ),0) as totale  FROM Prodotto  p left join dettaglio_comanda  dc on dc.prodotto_id=p.id   group by p.id,p.nome", nativeQuery = true)
    Collection<IProdottoTotale> prodottiVendutiTotali();



    @Query(value = "SELECT p.id as id  ,p.nome as nome ,COALESCE(SUM(dc.qta+dc.qta_o ),0) as totale  FROM Prodotto  p left join dettaglio_comanda  dc on dc.prodotto_id=p.id    where  dc.time>= :inizio and (dc.time<=:fine or dc.time=:fine) group by p.id,p.nome", nativeQuery = true)
    Collection<IProdottoTotale>listaProdottiVenduti( @Param("inizio") LocalDateTime inizio, @Param("fine") LocalDateTime fine);

    @Query(value = "SELECT p.id as id  ,p.nome as nome ,COALESCE(SUM(dc.qta+dc.qta_o ),0) as totale  FROM Prodotto  p left join dettaglio_comanda  dc on dc.prodotto_id=p.id    where  dc.time>= :inizio  group by p.id,p.nome", nativeQuery = true)
    Collection<IProdottoTotale>  listaProdottiVendutiDataFineNull( @Param("inizio") LocalDateTime inizio);

    @Query(value = "SELECT * FROM Prodotto as p join Categoria as c on c.id=p.categoria_id    WHERE  p.id = :id", nativeQuery = true)
     Categoria categoriaProdotto(@Param("id") Long id);

}
