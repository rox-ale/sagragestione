package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Ricetta;
import it.alessiorossato.sagriamo.model.Sessione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface SessioneDAO extends JpaRepository<Sessione, Long> {
    @Query(value = "SELECT * FROM sessione as s ", nativeQuery = true)
    public Set<Sessione> listaSessioni();


    @Modifying
    @Transactional
    @Query(value = "update sessione set data_ora_fine= :data_ora_fine where data_ora_fine is null ", nativeQuery = true)
    public void chiudiSessioni(@Param("data_ora_fine") LocalDateTime data_ora_fine);

    @Query(value = "SELECT * FROM sessione as s where s.data_ora_fine is null", nativeQuery = true)
    public Set<Sessione> listaSessioniAttive();

    @Query(value = "SELECT * FROM sessione as s where s.data_ora_fine is null", nativeQuery = true)
    public Sessione sessioneAttive();

}
