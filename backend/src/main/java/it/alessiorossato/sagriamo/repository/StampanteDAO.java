package it.alessiorossato.sagriamo.repository;

import it.alessiorossato.sagriamo.model.Stampante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface StampanteDAO extends JpaRepository<Stampante, Long> {
    //@Query(value = "SELECT * FROM Stampante WHERE abilitato=true ", nativeQuery = true)
    @Query(value = "SELECT * FROM Stampante  ", nativeQuery = true)
    public Set<Stampante> stampantiAttive();
}
