package it.alessiorossato.sagriamo.util;


import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.DettaglioComanda;

import java.util.HashSet;
import java.util.Set;

public class ComandaUtility {

    public static boolean comandaCucina(Set<DettaglioComanda> dc){
        boolean result=false;

        for(DettaglioComanda d:dc)
        {
            if(!d.getProdotto().getCategoria().isBar()){
                result=true;
            }
        }

        return result;
    }

    public static Set<DettaglioComanda> prodottiCategoria(Comanda c ,Set <Long> id){

        Set<DettaglioComanda> result=new HashSet<>();
        for(DettaglioComanda dc: c.getDettaglioComandas()){
            if(id.contains(dc.getProdotto().getCategoria().getReparto().getId())){
                result.add(dc);
            }
        }
        return result;
    }
}
