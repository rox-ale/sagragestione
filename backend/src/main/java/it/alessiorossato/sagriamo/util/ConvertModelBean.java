package it.alessiorossato.sagriamo.util;

import it.alessiorossato.sagriamo.bean.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConvertModelBean {

    public static Set<Categoria> convertiListaProdotti(Set<it.alessiorossato.sagriamo.model.Categoria> categoria) {
        Set<Categoria> nuovacategoria = new HashSet<>();
        // System.out.println("CONVERTI LISTA PRODOTTI");

        for (it.alessiorossato.sagriamo.model.Categoria c : categoria) {
            Categoria nc = new Categoria();
            nc.setId(c.getId());
            nc.setNome(c.getNome());
            nc.setAbilitato(c.isAbilitato());
            nc.setBar(c.isBar());
            nc.setReparto(convertiReparto(c.getReparto()));

            Set<Prodotto> nuovoProdotti = new HashSet<>();
            for (it.alessiorossato.sagriamo.model.Prodotto p : c.getProdottos()) {
                Prodotto pp = new Prodotto();
                pp.setId(p.getId());
                pp.setNome(p.getNome());
                pp.setPrezzo(p.getPrezzo());
                pp.setAbilitato(p.isAbilitato());
                // pp.setCategoria(nc);
                nuovoProdotti.add(pp);

            }

            nc.setProdottos(nuovoProdotti);
            nuovacategoria.add(nc);
            //  System.out.println("Nuovi prodotti "+nuovoProdotti);
        }
        return nuovacategoria;
    }


    public static Set<Cassa> convertiCassa(Set<it.alessiorossato.sagriamo.model.Cassa> cassa) {
        Set<Cassa> listaCasse = new HashSet<>();
        for (it.alessiorossato.sagriamo.model.Cassa c : cassa) {

            Cassa cc = new Cassa();
            cc.setAbilitato(c.isAbilitato());
            cc.setAsporto(c.isAsporto());
            cc.setBar(c.isBar());
            cc.setId(c.getId());
            cc.setNome(c.getNome());
            cc.setStampante(convertiStampante(c.getStampante()));
            listaCasse.add(cc);
        }

        return listaCasse;

    }

    public static Comanda convertiComanda(it.alessiorossato.sagriamo.model.Comanda comanda) {
        Comanda c = new Comanda();
        c.setId(comanda.getId());
        comanda.setCassa(comanda.getCassa());
        c.setCoperti(comanda.getCoperti());
        c.setDataOra(comanda.getDataOra());
        c.setOmaggio(comanda.getOmaggio());
        c.setResto(comanda.getResto());
        c.setTavolo(comanda.isTavolo());
        c.setTotale(comanda.getTotale());
        c.setVersato(comanda.getVersato());
        c.setNote(comanda.getNote());
        c.setConsegnato(comanda.getConsegnato());
        c.setPreparazione(comanda.getPreparazione());
        c.setRitirato(comanda.getRitirato());
        c.setProgressivo(comanda.getProgressivo());
        return c;
    }

    public static Cassa convertiCassa(it.alessiorossato.sagriamo.model.Cassa cassa) {
        Cassa c = new Cassa();
        c.setId(cassa.getId());
        c.setAbilitato(cassa.isAbilitato());
        c.setAsporto(cassa.isAsporto());
        c.setBar(cassa.isBar());
        c.setNome(cassa.getNome());
        c.setStampante(convertiStampante(cassa.getStampante()));
        //comande
        return c;
    }


    public static Stampante convertiStampante(it.alessiorossato.sagriamo.model.Stampante stampante) {
        Stampante s = new Stampante();
        s.setId(stampante.getId());
        s.setFormato(stampante.getFormato());
        s.setIp(stampante.getIp());
        s.setNome(stampante.getNome());
        s.setNomeStampantePc(stampante.getNomeStampantePc());

        return s;
    }


    public static Set<DettaglioComanda> convertiDettaglioComanda(Set<it.alessiorossato.sagriamo.model.DettaglioComanda> dettaglioComande) {
        Set<DettaglioComanda> listaDettaglioComande = new HashSet<>();

        for (it.alessiorossato.sagriamo.model.DettaglioComanda d : dettaglioComande) {

            DettaglioComanda dc = new DettaglioComanda();
            dc.setId(d.getId());
            dc.setPrezzo(d.getPrezzo());
            dc.setPrezzoTotale(d.getPrezzoTotale());
            dc.setProdotto(convertiProdotto(d.getProdotto()));
            dc.setQta(d.getQta());
            dc.setQtaO(d.getQtaO());
            dc.setTime(d.getTime());
            listaDettaglioComande.add(dc);

        }

        return listaDettaglioComande;
    }

    public static Set<Comanda> convertiComande(Set<it.alessiorossato.sagriamo.model.Comanda> comanda) {
        Set<Comanda> cc = new HashSet<>();

        for (it.alessiorossato.sagriamo.model.Comanda com : comanda) {
          /*  Comanda c=new Comanda();
            c.setId(com.getId());
           // c.setCassa(com.getCassa());
            c.setCoperti(com.getCoperti());
            c.setDataOra(com.getDataOra());
            c.setOmaggio(com.getOmaggio());
            c.setResto(com.getResto());
            c.setTavolo(com.isTavolo());
            c.setTotale(com.getTotale());
            c.setVersato(com.getVersato());
            c.setNote(com.getNote());*/
            Comanda comanda1 = convertiComanda(com);
            comanda1.setDettaglioComandas(convertiDettaglioComanda(com.getDettaglioComandas()));
            cc.add(comanda1);
        }
        return cc;
    }


    public static Set<Categoria> convertiCategoria(List<it.alessiorossato.sagriamo.model.Categoria> categoria) {
        Set<Categoria> nuovacategoria = new HashSet<>();
        // System.out.println("CONVERTI LISTA PRODOTTI");

        for (it.alessiorossato.sagriamo.model.Categoria c : categoria) {
            Categoria nc = new Categoria();
            nc.setId(c.getId());
            nc.setNome(c.getNome());
            nc.setAbilitato(c.isAbilitato());
            nc.setBar(c.isBar());
            nc.setReparto(convertiReparto(c.getReparto()));
            nuovacategoria.add(nc);
        }
        return nuovacategoria;
    }

    public static Categoria convertiCategoria(it.alessiorossato.sagriamo.model.Categoria c) {
        Categoria nc = new Categoria();
        nc.setId(c.getId());
        nc.setNome(c.getNome());
        nc.setAbilitato(c.isAbilitato());
        nc.setBar(c.isBar());
        nc.setReparto(convertiReparto(c.getReparto()));
        return nc;
    }


    public static Ingrediente convertiIngrediente(it.alessiorossato.sagriamo.model.Ingrediente ingrediente) {
        Ingrediente i = new Ingrediente();
        i.setId(ingrediente.getId());
        i.setGriglia(ingrediente.getGriglia());
        i.setNome(ingrediente.getNome());
        //mettere la ricetta

        return i;

    }


    public static Set<Ingrediente> convertiIngrediente(List<it.alessiorossato.sagriamo.model.Ingrediente> ingrediente) {
        Set<Ingrediente> cc = new HashSet<>();

        for (it.alessiorossato.sagriamo.model.Ingrediente i : ingrediente) {

            Ingrediente in = convertiIngrediente(i);
            cc.add(in);
        }
        return cc;
    }


    public static Prodotto convertiProdotto(it.alessiorossato.sagriamo.model.Prodotto prodotto) {
        Prodotto p = new Prodotto();
        p.setAbilitato(prodotto.isAbilitato());
        p.setId(prodotto.getId());
        p.setNome(prodotto.getNome());
        p.setPrezzo(prodotto.getPrezzo());

        p.setCategoria(convertiCategoria(prodotto.getCategoria()));
        p.setRicetta(convertiRicetta(prodotto.getRicetta()));
        return p;

    }


    public static Ricetta convertiRicetta(it.alessiorossato.sagriamo.model.Ricetta ricetta) {
        Ricetta r = new Ricetta();
        r.setId(ricetta.getId());
        r.setIngrediente(convertiIngrediente(ricetta.getIngrediente()));
        r.setQta(ricetta.getQta());
        return r;
    }

    public static Set<Ricetta> convertiRicetta(Set<it.alessiorossato.sagriamo.model.Ricetta> ricetta) {

        Set<Ricetta> listaRicetta = new HashSet<>();
        for (it.alessiorossato.sagriamo.model.Ricetta r : ricetta) {
            listaRicetta.add(convertiRicetta(r));
        }
        return listaRicetta;

    }

    public static Sessione convertiSessione(it.alessiorossato.sagriamo.model.Sessione sessione) {
        Sessione s = new Sessione();
        s.setDataFine(sessione.getDataFine());
        s.setDataInizio(sessione.getDataInizio());
        s.setId(sessione.getId());
        return s;
    }

    public static Set<Sessione> convertiSessione(Set<it.alessiorossato.sagriamo.model.Sessione> sessione) {
        Set<Sessione> listaSessione = new HashSet<>();
        for (it.alessiorossato.sagriamo.model.Sessione s : sessione) {
            listaSessione.add(convertiSessione(s));
        }
        return listaSessione;
    }


    public static Reparto convertiReparto(it.alessiorossato.sagriamo.model.Reparto reparto) {
        Reparto r = new Reparto();

        r.setAbilitato(reparto.getAbilitato());
        r.setId(reparto.getId());
        r.setNome(reparto.getNome());
        r.setStampante(convertiStampante(reparto.getStampante()));
        return r;
    }

    public static Set<Reparto> convertiReparto(Set<it.alessiorossato.sagriamo.model.Reparto> reparto) {
        Set<Reparto> listaReparti = new HashSet<>();
        for (it.alessiorossato.sagriamo.model.Reparto r : reparto) {
            listaReparti.add(convertiReparto(r));
        }
        return listaReparti;
    }


    public static Griglia convertiGriglia(it.alessiorossato.sagriamo.model.Griglia griglia) {

        Griglia g = new Griglia();
        g.setId(griglia.getId());
        g.setRimanenti(griglia.getRimanenti());
        g.setTotale(griglia.getTotale());
        g.setVenduti(griglia.getVenduti());

        return g;
    }

    public static Set<Griglia> convertiGriglia(List<it.alessiorossato.sagriamo.model.Griglia> griglia) {
        Set<Griglia> listaGriglia = new HashSet<>();
        for (it.alessiorossato.sagriamo.model.Griglia r : griglia) {
            listaGriglia.add(convertiGriglia(r));
        }
        return listaGriglia;
    }

}
