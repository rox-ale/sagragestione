package it.alessiorossato.sagriamo.util;

import java.time.LocalDateTime;
import java.time.Month;

public class Utility {

    /**
     * data usata per fare i confronti negli stati delle comande
     * */
    public static final LocalDateTime dataConfronto=LocalDateTime.of(2019, Month.MAY,  5,   10,   5);
}
