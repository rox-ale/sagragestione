package it.alessiorossato.sagriamo.util;

import it.alessiorossato.sagriamo.model.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConvertBeanModel {

    public static Comanda convertiComanda(it.alessiorossato.sagriamo.bean.Comanda comanda){
        Comanda c=new Comanda();
        c.setId(comanda.getId());
        c.setCassa(ConvertBeanModel.convertiCassa(comanda.getCassa()));
        c.setCoperti(comanda.getCoperti());
        c.setDataOra(comanda.getDataOra());
        c.setOmaggio(comanda.getOmaggio());
        c.setResto(comanda.getResto());
        c.setTavolo(comanda.getTavolo());
        c.setTotale(comanda.getTotale());
        c.setVersato(comanda.getVersato());
        c.setNote(comanda.getNote());
        c.setConsegnato(comanda.getConsegnato());
        c.setPreparazione(comanda.getPreparazione());
        c.setRitirato(comanda.getRitirato());
        c.setProgressivo(comanda.getProgressivo());
        return c;
    }



    public static DettaglioComanda convertiDettaglioComanda (it.alessiorossato.sagriamo.bean.DettaglioComanda dettaglioComanda){
        DettaglioComanda dc=new DettaglioComanda();
      //  dc.setComanda(dettaglioComanda.getComanda());
        dc.setId(dettaglioComanda.getId());
//        dc.setProdotto(ConvertBeanModel.convertiProdotto(dettaglioComanda.getProdotto()));
        dc.setPrezzo(dettaglioComanda.getPrezzo());
        dc.setPrezzoTotale(dettaglioComanda.getPrezzoTotale());
        dc.setQta(dettaglioComanda.getQta());
        dc.setQtaO(dettaglioComanda.getQtaO());
        dc.setTime(dettaglioComanda.getTime());
        return dc;
    }

    public static Prodotto  convertiProdotto(it.alessiorossato.sagriamo.bean.Prodotto prodotto){
        Prodotto p=new Prodotto();
    p.setId(prodotto.getId());
      //  p.setCategoria(ConvertBeanModel.convertiCategoria(prodotto.getCategoria()));
        p.setAbilitato(prodotto.getAbilitato());
        p.setNome(prodotto.getNome());
        p.setPrezzo(prodotto.getPrezzo());

        return p;
    }


    public static Ingrediente  convertiIngrediente(it.alessiorossato.sagriamo.bean.Ingrediente ingrediente){
     Ingrediente i=new Ingrediente();
     i.setGriglia(ingrediente.getGriglia());
     i.setId(ingrediente.getId());
     i.setNome(ingrediente.getNome());
     //ricetta

        return i;
    }


    public static Prodotto convertiProdottoCategoriaRicetta(it.alessiorossato.sagriamo.bean.Prodotto prodotto){
        Prodotto p=new Prodotto();
        p.setId(prodotto.getId());
        p.setCategoria(ConvertBeanModel.convertiCategoria(prodotto.getCategoria()));
        p.setAbilitato(prodotto.getAbilitato());
        p.setNome(prodotto.getNome());
        p.setPrezzo(prodotto.getPrezzo());
        p.setRicetta(convertiRicetta(prodotto.getRicetta()));
        Set<Ricetta> ricettaProdotto=p.getRicetta();
        for(Ricetta r:ricettaProdotto){
            r.setProdotto(convertiProdotto(prodotto));
        }

        return p;
    }

    public static Categoria convertiCategoria(it.alessiorossato.sagriamo.bean.Categoria categoria)
    {
        Categoria c=new Categoria();
        c.setId(categoria.getId());
        c.setAbilitato(categoria.getAbilitato());
        c.setBar(categoria.getBar());
        c.setNome(categoria.getNome());
        c.setReparto(convertiReparto(categoria.getReparto()));
        return c;
    }

    public static Cassa convertiCassa(it.alessiorossato.sagriamo.bean.Cassa cassa){
        Cassa c =new Cassa();
        c.setId(cassa.getId());
        c.setAbilitato(cassa.getAbilitato());
        c.setAsporto(cassa.getAsporto());
        c.setBar(cassa.getBar());
        c.setNome(cassa.getNome());
        c.setStampante(convertiStampante(cassa.getStampante()));
        //comande
        return c;
    }

    public static Stampante convertiStampante(it.alessiorossato.sagriamo.bean.Stampante stampante){
        Stampante s =new Stampante();
        s.setId(stampante.getId());
        s.setFormato(stampante.getFormato());
        s.setIp(stampante.getIp());
        s.setNome(stampante.getNome());
        s.setNomeStampantePc(stampante.getNomeStampantePc());

        return s;
    }




    public static Ricetta convertiRicetta (it.alessiorossato.sagriamo.bean.Ricetta ricetta){
        Ricetta r=new Ricetta();
        r.setId(ricetta.getId());
        r.setIngrediente(convertiIngrediente(ricetta.getIngrediente()));
        r.setQta(ricetta.getQta());
        return r;
    }
    public static Set< Ricetta> convertiRicetta (Set<it.alessiorossato.sagriamo.bean.Ricetta> ricetta){

        Set<Ricetta> listaRicetta=new HashSet<>();
        for(it.alessiorossato.sagriamo.bean.Ricetta r:ricetta){
            listaRicetta.add(convertiRicetta(r));
        }
        return listaRicetta;

    }


    public static it.alessiorossato.sagriamo.model.Reparto convertiReparto(it.alessiorossato.sagriamo.bean.Reparto reparto) {
        it.alessiorossato.sagriamo.model.Reparto r=new it.alessiorossato.sagriamo.model.Reparto();

        r.setAbilitato(reparto.getAbilitato());
        r.setId(reparto.getId());
        r.setNome(reparto.getNome());
        r.setStampante(convertiStampante(reparto.getStampante()));
        return r;
    }

    public static Set<it.alessiorossato.sagriamo.model.Reparto> convertiReparto(Set<it.alessiorossato.sagriamo.bean.Reparto> reparto) {
        Set<it.alessiorossato.sagriamo.model.Reparto> listaReparti = new HashSet<>();
        for (it.alessiorossato.sagriamo.bean.Reparto r : reparto) {
            listaReparti.add(convertiReparto(r));
        }
        return listaReparti;
    }



    public static Griglia convertiGriglia(it.alessiorossato.sagriamo.bean.Griglia griglia) {

        Griglia g = new Griglia();
        g.setId(griglia.getId());
        g.setRimanenti(griglia.getRimanenti());
        g.setTotale(griglia.getTotale());
        g.setVenduti(griglia.getVenduti());

        return g;
    }

    public static Set<Griglia> convertiGriglia(List<it.alessiorossato.sagriamo.bean.Griglia> griglia) {
        Set<Griglia> listaGriglia = new HashSet<>();
        for (it.alessiorossato.sagriamo.bean.Griglia r : griglia) {
            listaGriglia.add(convertiGriglia(r));
        }
        return listaGriglia;
    }
}
