package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.Sessione;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.DettaglioComandaDAO;
import it.alessiorossato.sagriamo.services.ComandaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;

@Service
public class ComandaServiceImp implements ComandaService {
    @Autowired
    DettaglioComandaDAO dettaglioComandaDAO;

    @Autowired
    ComandaDAO comandaDAO;



    public void eliminaComanda(Long id){

        dettaglioComandaDAO.cancellaDettaglioComanda(id);
        comandaDAO.cancellaComanda(id);
        //Optional<Comanda> c=comandaDAO.findById(id);
        //comandaDAO.deleteAll(c.get());

    }



    public double totaleCassa(Cassa cassa){
        Set<Comanda> listaComandeCassa = comandaDAO.listaComandeCassa(cassa.getId());
        double totale=0;
        for(Comanda c:listaComandeCassa){
            totale+=c.getTotale();
        }
        return totale;

    }


    public double totaleCassa(Cassa cassa, LocalDateTime inizio,LocalDateTime fine){
        double totale=0;

        Set<Comanda> listaComandeCassa = comandaDAO.listaComandeCassa(cassa.getId(), inizio, fine);
        for(Comanda c:listaComandeCassa){
            totale+=c.getTotale();
        }
        return totale;

    }

    public double totaleCassa(Cassa cassa, LocalDateTime inizio){
        double totale=0;

        Set<Comanda> listaComandeCassa = comandaDAO.listaComandeCassaDataFineNull(cassa.getId(), inizio);
        for(Comanda c:listaComandeCassa){
            totale+=c.getTotale();
        }
        return totale;

    }



    public Set<Comanda> comandeSessione(Sessione s){
        Set<Comanda> listaComandeSessione=null;
        if(s.getDataFine()==null){
             listaComandeSessione = comandaDAO.listaComandeSessioneDataFineNull(s.getDataInizio());
        }else{
            listaComandeSessione=comandaDAO.listaComandeSessione(s.getDataInizio(),s.getDataFine());
        }

        return listaComandeSessione;
    }
}
