package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.bean.Storico;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.repository.*;
import it.alessiorossato.sagriamo.services.StoricoService;
import it.alessiorossato.sagriamo.util.ComandaUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class StoricoServiceImp implements StoricoService {
    @Autowired
    ComandaDAO comandaDAO;

    @Autowired
    DettaglioComandaDAO dettaglioComandaDAO;

    public List<Storico> allComanda(){
        // List<Comanda> a=   comandaRepository.findAll();
        // List<Object[]> a=comandaDAO.listaComande();
        List<Comanda>      a=comandaDAO.findAll();
        // System.out.println("Storico "+comandaDAO.findAll().toString());

        List<Storico> storico=new ArrayList<>();
        for(Comanda c:a){
            Storico s=new Storico();
            s.setId(c.getId());
            s.setCassa(c.getCassa().getNome());
            s.setDataOra(c.getDataOra());
            s.setProgressivo(c.getProgressivo());

            boolean result=(ComandaUtility.comandaCucina(dettaglioComandaDAO.prodottiComanda(c.getId())));
            s.setCucina(result);
            storico.add(s);
        }


        return storico;
    }


    public List<Storico> allComandaCassa(Long id){
        // List<Comanda> a=   comandaRepository.findAll();
        // List<Object[]> a=comandaDAO.listaComande();
        Set<Comanda> comandas = comandaDAO.listaComandeCassa(id);

        for(Comanda c:comandas)
        {
            c.setDettaglioComandas(dettaglioComandaDAO.prodottiComanda(c.getId()));
        }
        // System.out.println("Storico "+comandaDAO.findAll().toString());

        List<Storico> storico=new ArrayList<>();
        for(Comanda c:comandas){
            Storico s=new Storico();
            s.setId(c.getId());
            s.setCassa(c.getCassa().getNome());
            s.setDataOra(c.getDataOra());
            s.setProgressivo(c.getProgressivo());

            boolean result=(ComandaUtility.comandaCucina(dettaglioComandaDAO.prodottiComanda(c.getId())));
            s.setCucina(result);
            storico.add(s);
        }


        return storico;
    }


}
