package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Comanda;

import java.io.Serializable;
import java.util.Set;

public interface CucinaService extends Serializable {
    Set<Comanda> allComandaDaFare();

    void comandaPresaInCarico(Long id);

    void comandaInDistribuzione(Long id);
    void comandaRitirata(Long id);

    //  void eliminaComanda(Long id);
}
