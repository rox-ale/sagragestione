package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.repository.CategoriaDAO;
import it.alessiorossato.sagriamo.services.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class CategoriaServiceImp implements CategoriaService {

    @Autowired
    CategoriaDAO categoriaDAO;

    public  List<Categoria>  listaCategorie(){
        List<Categoria> all = categoriaDAO.findAll();
        return all;
    }

    @Override
    public List<Categoria> salva(Set<Categoria> lista) {
        for(Categoria c:lista){
            categoriaDAO.save(c);
        }
        return categoriaDAO.findAll();
    }
}
