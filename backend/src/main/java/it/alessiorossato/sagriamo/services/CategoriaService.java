package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Categoria;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface CategoriaService extends Serializable {
     List<Categoria> listaCategorie();

     List<Categoria> salva(Set<Categoria> lista);
}
