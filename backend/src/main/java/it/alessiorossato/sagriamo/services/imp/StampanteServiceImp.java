package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Stampante;
import it.alessiorossato.sagriamo.repository.StampanteDAO;
import it.alessiorossato.sagriamo.services.StampanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class StampanteServiceImp implements StampanteService {
    @Autowired
    StampanteDAO stampanteDAO;

    public Set<Stampante> listaStampanti(){

        Set<Stampante> s = stampanteDAO.stampantiAttive();
        return s;
    }

    @Override
    public List<Stampante> salva(Set<Stampante> lista) {
        for(Stampante c:lista){
            stampanteDAO.save(c);
        }
        return stampanteDAO.findAll();
    }
}
