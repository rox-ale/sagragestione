package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Stampante;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface StampanteService extends Serializable {
    Set<Stampante> listaStampanti();
    List<Stampante> salva(Set<Stampante> lista);
}
