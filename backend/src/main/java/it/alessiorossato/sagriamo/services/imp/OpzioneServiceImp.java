package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Ingrediente;
import it.alessiorossato.sagriamo.model.Opzione;
import it.alessiorossato.sagriamo.repository.IngredienteDAO;
import it.alessiorossato.sagriamo.repository.OpzioneDAO;
import it.alessiorossato.sagriamo.services.IngredienteService;
import it.alessiorossato.sagriamo.services.OpzioneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class OpzioneServiceImp implements OpzioneService {

    @Autowired
    OpzioneDAO opzioneDAO;

    public Opzione progressivo(){
        Opzione opzione = opzioneDAO.opzioneProgressivo();
        return opzione;
    }

    public synchronized  String getProgressivo(){
      Opzione progressivo=  progressivo();
      String valore=progressivo.getValore();
      int valoreInt=Integer.parseInt(valore);
      progressivo.setValore(""+(valoreInt+1));
        Opzione save = opzioneDAO.save(progressivo);

        return save.getValore();
    }

}

