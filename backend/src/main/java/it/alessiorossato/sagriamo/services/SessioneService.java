package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Sessione;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface SessioneService extends Serializable {

    public Set<Sessione> listaSessioni();
    public void chiudiSessioni();
    public void apriSessione();
    public boolean verificaSessioneAttiva();
    public Sessione sessioneAttiva();
}
