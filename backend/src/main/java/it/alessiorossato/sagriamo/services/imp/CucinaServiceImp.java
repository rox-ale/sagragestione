package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.DettaglioComandaDAO;
import it.alessiorossato.sagriamo.services.CucinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Service
public class CucinaServiceImp implements CucinaService {

    @Autowired
    ComandaDAO comandaDAO;

    @Autowired
    DettaglioComandaDAO dettaglioComandaDAO;

    @Override
    public Set<Comanda> allComandaDaFare() {
        Set<Comanda> comandas = comandaDAO.listaComandeDaFare();
        return comandas;
    }

    @Override
    public void comandaPresaInCarico(Long id) {
      //  LocalDate ld = LocalDate.now();
        //System.out.println("Data "+ld);

        Optional<Comanda> comandaO = comandaDAO.findById(id);

        LocalDateTime dataOra= LocalDateTime.now();
      Comanda c=comandaO.get();

        c.setPreparazione(dataOra);

 comandaDAO.save(c);
     //   comandaDAO.presaInCarico(id, ld);
    }



    @Override
    public void comandaInDistribuzione(Long id) {
        //  LocalDate ld = LocalDate.now();
        //System.out.println("Data "+ld);

        Optional<Comanda> comandaO = comandaDAO.findById(id);

        LocalDateTime dataOra= LocalDateTime.now();
        Comanda c=comandaO.get();

        c.setConsegnato(dataOra);

        comandaDAO.save(c);
        //   comandaDAO.presaInCarico(id, ld);
    }

    @Override
    public void comandaRitirata(Long id) {
        //  LocalDate ld = LocalDate.now();
        //System.out.println("Data "+ld);

        Optional<Comanda> comandaO = comandaDAO.findById(id);

        LocalDateTime dataOra= LocalDateTime.now();
        Comanda c=comandaO.get();

        c.setRitirato(dataOra);

        comandaDAO.save(c);
        //   comandaDAO.presaInCarico(id, ld);
    }

}
