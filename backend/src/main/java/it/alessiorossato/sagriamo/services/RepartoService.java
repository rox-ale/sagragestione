package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Opzione;
import it.alessiorossato.sagriamo.model.Reparto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface RepartoService extends Serializable {
     List<Reparto> listaReaprti();
     public List<Reparto> salva(Set<Reparto> lista);
}
