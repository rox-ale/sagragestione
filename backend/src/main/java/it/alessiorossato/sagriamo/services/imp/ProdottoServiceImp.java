package it.alessiorossato.sagriamo.services.imp;


import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.DettaglioComanda;
import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.repository.ProdottoDAO;
import it.alessiorossato.sagriamo.repository.query.IProdottoTotale;
import it.alessiorossato.sagriamo.services.ProdottoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional
public class ProdottoServiceImp implements ProdottoService {

    @Autowired
    ProdottoDAO prodottoDAO;


    public List<Prodotto> listaProdotti(){
        return prodottoDAO.findAll();
    }


    @Override
    public List<Prodotto> salva(Set<Prodotto> lista) {
      //  List<Prodotto> p=lista.stream().collect(Collectors.toList());

                //(List<Prodotto>)lista;
        for(Prodotto c:lista){
             prodottoDAO.save(c);
        }
        return prodottoDAO.findAll();
    }





    public Collection<IProdottoTotale> totaleProdotti(){

        Collection<IProdottoTotale> lista= prodottoDAO.prodottiVendutiTotali();
       return lista;
    }



    public Collection<IProdottoTotale> totaleCassa(LocalDateTime inizio, LocalDateTime fine){
       return prodottoDAO.listaProdottiVenduti(inizio,fine);

    }

    public  Collection<IProdottoTotale> totaleCassa( LocalDateTime inizio){
       return prodottoDAO.listaProdottiVendutiDataFineNull(inizio);

    }

    public Categoria categoriaProdotto(Prodotto p){
        /*Optional<Prodotto> prodotto = prodottoDAO.findById(p.getId());
        if(prodotto.isPresent()){
            Prodotto prodotto1 = prodotto.get();
            return prodotto1.getCategoria();
        }
        return null;*/

        return prodottoDAO.categoriaProdotto(p.getId());
    }


    public List<DettaglioComanda> prodottiReparto(Comanda comanda, Set<Long> idReparti){

        List<DettaglioComanda> result=new ArrayList<>();
        for(DettaglioComanda dc:comanda.getDettaglioComandas()){
            Optional<Prodotto> p=prodottoDAO.findById(dc.getProdotto().getId());
            if(p.isPresent()) {
                if (idReparti.contains(p.get().getCategoria().getReparto())) {
                    dc.setProdotto(p.get());
                    result.add(dc);
                }
            }
        }
        return result;
    }
}
