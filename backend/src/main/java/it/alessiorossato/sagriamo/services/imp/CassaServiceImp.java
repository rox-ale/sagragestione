package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.repository.CassaDAO;
import it.alessiorossato.sagriamo.repository.CategoriaDAO;
import it.alessiorossato.sagriamo.repository.ProdottoDAO;
import it.alessiorossato.sagriamo.services.CassaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CassaServiceImp implements CassaService {

    @Autowired
    private CassaDAO cassaDAO;

    @Autowired
    private CategoriaDAO categoriaDAO;


    @Autowired
    private ProdottoDAO prodottoDAO;

    public Set<Cassa> listacasse(){
        System.out.println("Lista casse "+cassaDAO.cassaAttive());
        return cassaDAO.cassaAttive();
    }

    public Set<Categoria> listaProdotti(boolean bar){
        Set<Categoria> categoria;
        if(bar==true){
            categoria=categoriaDAO.categorieAttive(bar);
        }else{
         categoria=categoriaDAO.categorieAttive();
        }

        System.out.println("Categorie: "+categoria);
        for(Categoria c:categoria){
            //  System.out.println("Categoria da service "+c.getId());

            Set<Prodotto> p=prodottoDAO.prodottiAttiviCategoria(c.getId());
            // System.out.println("Prodotto "+p);
            c.setProdottos(p);

        }

        //stampo la lista di categoria e prodotto
        //  System.out.println("Lista categoria");
       /* for(Categoria c :categoria){
            System.out.println("Categoria "+c);
            for(Prodotto p:c.getProdottos()){
                System.out.println("Prodotto"+p);
            }
        }*/
        //    System.out.println(categoria);


        return categoria;
    }

    @Override
    public Set<Cassa> listaCasseTutte() {
        Set<Cassa> result=new HashSet<>();
        List<Cassa> all = cassaDAO.findAll();
        for(Cassa c:all){
            result.add(c);

        }
        return result;
    }


    @Override
    public List<Cassa> salva(Set<Cassa> lista) {
        for(Cassa c:lista){
            cassaDAO.save(c);
        }
        return cassaDAO.findAll();
    }
}
