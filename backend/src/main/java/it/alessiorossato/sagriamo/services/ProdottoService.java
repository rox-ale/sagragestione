package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Categoria;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.DettaglioComanda;
import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.repository.query.IProdottoTotale;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ProdottoService extends Serializable {
    List<Prodotto> listaProdotti();
    List<Prodotto> salva(Set<Prodotto> lista);
    public Collection<IProdottoTotale> totaleProdotti();


    public Collection<IProdottoTotale> totaleCassa(LocalDateTime inizio, LocalDateTime fine);

    public  Collection<IProdottoTotale> totaleCassa( LocalDateTime inizio);
    public Categoria categoriaProdotto(Prodotto p);
    List<DettaglioComanda> prodottiReparto(Comanda comanda, Set<Long> idReparti);
}
