package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Comanda;
import it.alessiorossato.sagriamo.model.Sessione;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

public interface ComandaService extends Serializable {
    void eliminaComanda(Long id);
    public double totaleCassa(Cassa cassa);
    public double totaleCassa(Cassa cassa, LocalDateTime inizio, LocalDateTime fine);
    public double totaleCassa(Cassa cassa, LocalDateTime inizio);
    public Set<Comanda> comandeSessione(Sessione s);
}
