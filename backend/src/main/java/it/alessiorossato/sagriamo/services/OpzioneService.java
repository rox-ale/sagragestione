package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Ingrediente;
import it.alessiorossato.sagriamo.model.Opzione;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface OpzioneService extends Serializable {
     public Opzione progressivo();
     public String getProgressivo();
}
