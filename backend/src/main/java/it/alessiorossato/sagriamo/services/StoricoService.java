package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.bean.Storico;

import java.io.Serializable;
import java.util.List;

public interface StoricoService extends Serializable {
    List<Storico> allComanda();

    List<Storico> allComandaCassa(Long id);
}
