package it.alessiorossato.sagriamo.services.imp;


import it.alessiorossato.sagriamo.model.Griglia;
import it.alessiorossato.sagriamo.model.Ingrediente;

import it.alessiorossato.sagriamo.repository.GrigliaDAO;
import it.alessiorossato.sagriamo.repository.IngredienteDAO;
import it.alessiorossato.sagriamo.services.IngredienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class IngredienteServiceImp implements IngredienteService {

    @Autowired
    IngredienteDAO ingredienteDAO;

   @Autowired
   GrigliaDAO grigliaDAO;

    public List<Ingrediente>  listaIngredienti(){
        List<Ingrediente> all = ingredienteDAO.findAll();
        return all;
    }


    @Override
    public List<Ingrediente> salva(Set<Ingrediente> lista) {
        for(Ingrediente c:lista){
            ingredienteDAO.save(c);


            //verifico se è già inserito
            Optional<Griglia> griglia = grigliaDAO.findById(c.getId());
            if(griglia.isPresent()){
                //verifico se il flag griglia sia a true o meno
                //se è true non faccio niente, se è false lo elimino

                if(c.getGriglia()){
                    //true  non faccio niente
                }else{
                    //lo elimino

                    grigliaDAO.delete(griglia.get());
                }

            }else {

                //non è presente, verifico se devo inserirlo in base al flag

                if(c.getGriglia()){
                    //lo inserisco
                    Griglia g=new Griglia();
                    g.setId(c.getId());

                    grigliaDAO.save(g);
                }


            }

        }
        return ingredienteDAO.findAll();
    }
}

