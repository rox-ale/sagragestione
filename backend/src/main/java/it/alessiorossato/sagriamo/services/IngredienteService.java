package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Ingrediente;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface IngredienteService extends Serializable {
     List<Ingrediente>  listaIngredienti();
     List<Ingrediente> salva(Set<Ingrediente> lista);
}
