package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Cassa;
import it.alessiorossato.sagriamo.model.Categoria;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface CassaService extends Serializable {
    Set<Cassa> listacasse();
    Set<Categoria> listaProdotti(boolean bar);

    Set<Cassa> listaCasseTutte();
    List<Cassa> salva(Set<Cassa> lista);
}
