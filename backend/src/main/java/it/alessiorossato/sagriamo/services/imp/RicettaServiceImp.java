package it.alessiorossato.sagriamo.services.imp;


import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.model.Ricetta;
import it.alessiorossato.sagriamo.repository.RicettaDAO;
import it.alessiorossato.sagriamo.services.RicettaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class RicettaServiceImp implements RicettaService {

    @Autowired
    RicettaDAO ricettaDao;


    @Override
    public List<Ricetta> salva(Set<Ricetta> lista) {
      //  List<Prodotto> p=lista.stream().collect(Collectors.toList());

                //(List<Prodotto>)lista;
        for(Ricetta c:lista){
            ricettaDao.save(c);
        }


        return ricettaDao.findAll();
    }

    @Override
    public Set<Ricetta> eliminaRicettaEliminatiIngredienti(Set<Ricetta> lista,Prodotto prodotto){
        Set<Ricetta> ricettas = ricettaDao.ricettaProdotto(prodotto.getId());

        for(Ricetta ricetta:ricettas){
            /*if(lista.contains(ricetta)){
                //ok non faccio niente
            }else{
                ricettaDao.delete(ricetta);
            }*/

            if(!verificaPresenzaProdotto(lista,ricetta)){
                ricettaDao.delete(ricetta);
            }
        }

        return  ricettaDao.ricettaProdotto(prodotto.getId());
    }
    @Override
    public Set<Ricetta> listaRicettaProdotto(Long idProdotto){
     return  ricettaDao.ricettaProdotto(idProdotto);
    }



    private boolean verificaPresenzaProdotto(Set<Ricetta> lista,Ricetta ricetta){
        for(Ricetta r:lista){
            if(r.getId()==ricetta.getId()){
                return true;
            }
        }
        return false;
    }
}
