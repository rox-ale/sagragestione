package it.alessiorossato.sagriamo.services;

import it.alessiorossato.sagriamo.model.Prodotto;
import it.alessiorossato.sagriamo.model.Ricetta;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface RicettaService extends Serializable {
    public List<Ricetta> salva(Set<Ricetta> lista);
    public Set<Ricetta> listaRicettaProdotto(Long idProdotto);
    public Set<Ricetta> eliminaRicettaEliminatiIngredienti(Set<Ricetta> lista,Prodotto prodotto);
}
