package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.*;
import it.alessiorossato.sagriamo.repository.ComandaDAO;
import it.alessiorossato.sagriamo.repository.OpzioneDAO;
import it.alessiorossato.sagriamo.repository.RepartoDAO;
import it.alessiorossato.sagriamo.repository.SessioneDAO;
import it.alessiorossato.sagriamo.services.RepartoService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class RepartoServiceImp implements RepartoService {

   @Autowired
    RepartoDAO repartoDAO;

    public List<Reparto> listaReaprti(){
        List<Reparto> repartoList = repartoDAO.findAll();
        return repartoList;
    }

    @Override
    public List<Reparto> salva(Set<Reparto> lista) {
        for(Reparto c:lista){
            repartoDAO.save(c);
        }
        return repartoDAO.findAll();
    }
}
