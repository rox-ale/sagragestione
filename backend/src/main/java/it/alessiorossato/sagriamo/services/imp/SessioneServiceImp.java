package it.alessiorossato.sagriamo.services.imp;

import it.alessiorossato.sagriamo.model.*;
import it.alessiorossato.sagriamo.repository.*;
import it.alessiorossato.sagriamo.services.CassaService;
import it.alessiorossato.sagriamo.services.SessioneService;
import it.alessiorossato.sagriamo.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SessioneServiceImp implements SessioneService {

    @Autowired
    SessioneDAO sessioneDAO;

    @Autowired
    ComandaDAO comandaDAO;

    @Autowired
    OpzioneDAO opzioneDAO;

    public Set<Sessione> listaSessioni(){
      return   sessioneDAO.listaSessioni();
    }

    public void chiudiSessioni(){
        LocalDateTime dataFine=LocalDateTime.now();
        sessioneDAO.chiudiSessioni(dataFine);


        /*
        cerco tutte le comande che non sono state gestite, ad esempio tutte quelle del bar ed evventuali residui della cucina.
        controllo dalla data
         */

        Set<Comanda> listaComandeDaFare = comandaDAO.listaComandeDaFare();
        for(Comanda comanda:listaComandeDaFare){
            if(comanda.getConsegnato().isEqual( Utility.dataConfronto)){
                comanda.setConsegnato(dataFine);
            }
            if(comanda.getPreparazione().isEqual(Utility.dataConfronto)){
                comanda.setPreparazione(dataFine);
            }
            if(comanda.getRitirato().isEqual(Utility.dataConfronto)){
                comanda.setRitirato(dataFine);
            }

            comandaDAO.save(comanda);
        }
    }


    public void apriSessione(){
        Sessione sessione=new Sessione();
        LocalDateTime dataInizio=LocalDateTime.now();
        sessione.setDataInizio(dataInizio);

        Opzione opzione=opzioneDAO.opzioneProgressivo();
        opzione.setValore("0");
        opzioneDAO.save(opzione);
        sessioneDAO.save(sessione);
    }

    @Override
    public boolean verificaSessioneAttiva() {
        Set<Sessione> listaSessioniAttive = sessioneDAO.listaSessioniAttive();
        if(!listaSessioniAttive.isEmpty()){
            return true;
        }

        return false;
    }

    @Override
    public Sessione sessioneAttiva() {

        Sessione sessione=sessioneDAO.sessioneAttive();
        return sessione;
    }
}
