package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

@Entity
@Table(name = "opzione")
public class Opzione implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "valore")
    private String valore;

    @Column(name = "descrizione")
    private String descrizione;

    public Opzione() {
    }

    public Opzione(String nome, String valore, String descrizione) {
        this.nome = nome;
        this.valore = valore;
        this.descrizione = descrizione;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValore() {
        return valore;
    }

    public void setValore(String valore) {
        this.valore = valore;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Opzione opzione = (Opzione) o;
        return Objects.equals(id, opzione.id) &&
                Objects.equals(nome, opzione.nome) &&
                Objects.equals(valore, opzione.valore) &&
                Objects.equals(descrizione, opzione.descrizione);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, valore, descrizione);
    }

    @Override
    public String toString() {
        return "Opzione{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", valore='" + valore + '\'' +
                ", descrizione='" + descrizione + '\'' +
                '}';
    }
}
