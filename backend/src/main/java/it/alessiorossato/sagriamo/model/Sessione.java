package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Cassa.
 */
@Entity
@Table(name = "sessione")
public class Sessione implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "data_ora_inizio")
    private LocalDateTime dataInizio;

    @Column(name = "data_ora_fine")
    private LocalDateTime dataFine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(LocalDateTime dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDateTime getDataFine() {
        return dataFine;
    }

    public void setDataFine(LocalDateTime dataFine) {
        this.dataFine = dataFine;
    }

    @Override
    public String toString() {
        return "Sessione{" +
                "id=" + id +
                ", dataInizio=" + dataInizio +
                ", dataFine=" + dataFine +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sessione sessione = (Sessione) o;
        return Objects.equals(id, sessione.id) &&
                Objects.equals(dataInizio, sessione.dataInizio) &&
                Objects.equals(dataFine, sessione.dataFine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataInizio, dataFine);
    }
}
