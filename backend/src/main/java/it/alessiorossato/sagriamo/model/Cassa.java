package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Cassa.
 */
@Entity
@Table(name = "cassa")
public class Cassa implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "abilitato")
    private Boolean abilitato;

    @Column(name = "bar")
    private Boolean bar;

    @Column(name = "asporto")
    private Boolean asporto;

    @OneToMany(mappedBy = "cassa")
    private Set<Comanda> comandas = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("cassas")
    private Stampante stampante;

    @ManyToMany
    private Set<Reparto> repartos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Cassa nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean isAbilitato() {
        return abilitato;
    }

    public Cassa abilitato(Boolean abilitato) {
        this.abilitato = abilitato;
        return this;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Boolean isBar() {
        return bar;
    }

    public Cassa bar(Boolean bar) {
        this.bar = bar;
        return this;
    }

    public void setBar(Boolean bar) {
        this.bar = bar;
    }

    public Boolean isAsporto() {
        return asporto;
    }

    public Cassa asporto(Boolean asporto) {
        this.asporto = asporto;
        return this;
    }

    public void setAsporto(Boolean asporto) {
        this.asporto = asporto;
    }

    public Set<Comanda> getComandas() {
        return comandas;
    }

    public Cassa comandas(Set<Comanda> comandas) {
        this.comandas = comandas;
        return this;
    }

    public Cassa addComanda(Comanda comanda) {
        this.comandas.add(comanda);
        comanda.setCassa(this);
        return this;
    }

    public Cassa removeComanda(Comanda comanda) {
        this.comandas.remove(comanda);
        comanda.setCassa(null);
        return this;
    }

    public void setComandas(Set<Comanda> comandas) {
        this.comandas = comandas;
    }

    public Stampante getStampante() {
        return stampante;
    }

    public Cassa stampante(Stampante stampante) {
        this.stampante = stampante;
        return this;
    }

    public void setStampante(Stampante stampante) {
        this.stampante = stampante;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Set<Reparto> getRepartos() {
        return repartos;
    }

    public void setRepartos(Set<Reparto> repartos) {
        this.repartos = repartos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cassa cassa = (Cassa) o;
        if (cassa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cassa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cassa{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", abilitato='" + isAbilitato() + "'" +
            ", bar='" + isBar() + "'" +
            ", asporto='" + isAsporto() + "'" +
            "}";
    }
}
