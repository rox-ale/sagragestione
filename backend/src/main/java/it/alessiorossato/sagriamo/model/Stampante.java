package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Stampante.
 */
@Entity
@Table(name = "stampante")
public class Stampante implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "ip")
    private String ip;

    @Column(name = "formato")
    private String formato;

    @Column(name = "nome_stampante_pc")
    private String nomeStampantePc;

    @OneToMany(mappedBy = "stampante")
    private Set<Cassa> cassas = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "stampante")
    private Reparto reparto;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Stampante nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIp() {
        return ip;
    }

    public Stampante ip(String ip) {
        this.ip = ip;
        return this;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFormato() {
        return formato;
    }

    public Stampante formato(String formato) {
        this.formato = formato;
        return this;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNomeStampantePc() {
        return nomeStampantePc;
    }

    public Stampante nomeStampantePc(String nomeStampantePc) {
        this.nomeStampantePc = nomeStampantePc;
        return this;
    }

    public void setNomeStampantePc(String nomeStampantePc) {
        this.nomeStampantePc = nomeStampantePc;
    }

    public Set<Cassa> getCassas() {
        return cassas;
    }

    public Stampante cassas(Set<Cassa> cassas) {
        this.cassas = cassas;
        return this;
    }

    public Stampante addCassa(Cassa cassa) {
        this.cassas.add(cassa);
        cassa.setStampante(this);
        return this;
    }

    public Stampante removeCassa(Cassa cassa) {
        this.cassas.remove(cassa);
        cassa.setStampante(null);
        return this;
    }

    public void setCassas(Set<Cassa> cassas) {
        this.cassas = cassas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Stampante stampante = (Stampante) o;
        if (stampante.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), stampante.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Stampante{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", ip='" + getIp() + "'" +
            ", formato='" + getFormato() + "'" +
            ", nomeStampantePc='" + getNomeStampantePc() + "'" +
            "}";
    }
}
