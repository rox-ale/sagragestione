package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DettaglioComanda.
 */
@Entity
@Table(name = "dettaglio_comanda")
public class DettaglioComanda implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "qta")
    private Integer qta;

    @Column(name = "qta_o")
    private Integer qtaO;

    @Column(name = "time")
    private LocalDateTime time;

    @Column(name = "prezzo")
    private Double prezzo;

    @Column(name = "prezzo_totale")
    private Double prezzoTotale;

    @ManyToOne
    @JsonIgnoreProperties("dettaglioComandas")
    private Prodotto prodotto;

    @ManyToOne
    @JsonIgnoreProperties("dettaglioComandas")
    private Comanda comanda;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQta() {
        return qta;
    }

    public DettaglioComanda qta(Integer qta) {
        this.qta = qta;
        return this;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    public Integer getQtaO() {
        return qtaO;
    }

    public DettaglioComanda qtaO(Integer qtaO) {
        this.qtaO = qtaO;
        return this;
    }

    public void setQtaO(Integer qtaO) {
        this.qtaO = qtaO;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public DettaglioComanda time(LocalDateTime time) {
        this.time = time;
        return this;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public DettaglioComanda prezzo(Double prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public Double getPrezzoTotale() {
        return prezzoTotale;
    }

    public DettaglioComanda prezzoTotale(Double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
        return this;
    }

    public void setPrezzoTotale(Double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public DettaglioComanda prodotto(Prodotto prodotto) {
        this.prodotto = prodotto;
        return this;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public Comanda getComanda() {
        return comanda;
    }

    public DettaglioComanda comanda(Comanda comanda) {
        this.comanda = comanda;
        return this;
    }

    public void setComanda(Comanda comanda) {
        this.comanda = comanda;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DettaglioComanda dettaglioComanda = (DettaglioComanda) o;
        if (dettaglioComanda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dettaglioComanda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DettaglioComanda{" +
            "id=" + getId() +
            ", qta=" + getQta() +
            ", qtaO=" + getQtaO() +
            ", time='" + getTime() + "'" +
            ", prezzo=" + getPrezzo() +
            ", prezzoTotale=" + getPrezzoTotale() +
            "}";
    }
}
