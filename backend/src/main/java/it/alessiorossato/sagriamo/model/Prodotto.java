package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Prodotto.
 */
@Entity
@Table(name = "prodotto")
public class Prodotto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "prezzo")
    private Double prezzo;

    @Column(name = "abilitato")
    private Boolean abilitato;

    @ManyToOne
   // @JsonIgnoreProperties("prodottos")
    private Categoria categoria;

    @OneToMany (mappedBy = "prodotto")
    //@JsonIgnoreProperties("prodotto")
    private Set<Ricetta> ricetta=new HashSet<>();

    @OneToMany(mappedBy = "prodotto")
    private Set<DettaglioComanda> dettaglioComandas = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Prodotto nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public Prodotto prezzo(Double prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public Boolean isAbilitato() {
        return abilitato;
    }

    public Prodotto abilitato(Boolean abilitato) {
        this.abilitato = abilitato;
        return this;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Prodotto categoria(Categoria categoria) {
        this.categoria = categoria;
        return this;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Set<DettaglioComanda> getDettaglioComandas() {
        return dettaglioComandas;
    }

    public Prodotto dettaglioComandas(Set<DettaglioComanda> dettaglioComandas) {
        this.dettaglioComandas = dettaglioComandas;
        return this;
    }

    public Prodotto addDettaglioComanda(DettaglioComanda dettaglioComanda) {
        this.dettaglioComandas.add(dettaglioComanda);
        dettaglioComanda.setProdotto(this);
        return this;
    }

    public Prodotto removeDettaglioComanda(DettaglioComanda dettaglioComanda) {
        this.dettaglioComandas.remove(dettaglioComanda);
        dettaglioComanda.setProdotto(null);
        return this;
    }

    public void setDettaglioComandas(Set<DettaglioComanda> dettaglioComandas) {
        this.dettaglioComandas = dettaglioComandas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getAbilitato() {
        return abilitato;
    }

    public Set<Ricetta> getRicetta() {
        return ricetta;
    }

    public void setRicetta(Set<Ricetta> ricetta) {
        this.ricetta = ricetta;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Prodotto{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", prezzo=" + getPrezzo() +
            ", abilitato='" + isAbilitato() + "'" +
            "}";
    }
}
