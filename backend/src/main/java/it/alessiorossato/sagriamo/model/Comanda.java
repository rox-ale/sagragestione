package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Comanda.
 */
@Entity
@Table(name = "comanda")
public class Comanda implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "progressivo")
    private Integer progressivo;

    @Column(name = "data_ora")
    private LocalDateTime dataOra;

    @Column(name = "tavolo")
    private Boolean tavolo;

    @Column(name = "totale")
    private Double totale;

    @Column(name = "versato")
    private Double versato;

    @Column(name = "resto")
    private Double resto;

    @Column(name = "coperti")
    private Integer coperti;

    @Column(name = "omaggio")
    private Integer omaggio;

    @Column(name = "note")
    private String note;

    @Column(name = "consegnato"/*, columnDefinition="LocalDateTime default '2019-05-05 00:00:00'"*/)
    private LocalDateTime consegnato;

    @Column(name = "preparazione"/*, columnDefinition="LocalDateTime default '2019-05-05 00:00:00'"*/)
    private LocalDateTime preparazione;

    @Column(name = "ritirato"/*, columnDefinition="LocalDateTime default '2019-05-05 00:00:00'"*/)
    private LocalDateTime ritirato;

    @ManyToOne
    @JsonIgnoreProperties("comandas")
    private Cassa cassa;

    @OneToMany(mappedBy = "comanda")
    private Set<DettaglioComanda> dettaglioComandas = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProgressivo() {
        return progressivo;
    }

    public Comanda progressivo(Integer progressivo) {
        this.progressivo = progressivo;
        return this;
    }

    public void setProgressivo(Integer progressivo) {
        this.progressivo = progressivo;
    }

    public LocalDateTime getDataOra() {
        return dataOra;
    }

    public Comanda dataOra(LocalDateTime dataOra) {
        this.dataOra = dataOra;
        return this;
    }

    public void setDataOra(LocalDateTime dataOra) {
        this.dataOra = dataOra;
    }

    public Boolean isTavolo() {
        return tavolo;
    }

    public Comanda tavolo(Boolean tavolo) {
        this.tavolo = tavolo;
        return this;
    }

    public void setTavolo(Boolean tavolo) {
        this.tavolo = tavolo;
    }

    public Double getTotale() {
        return totale;
    }

    public Comanda totale(Double totale) {
        this.totale = totale;
        return this;
    }

    public void setTotale(Double totale) {
        this.totale = totale;
    }

    public Double getVersato() {
        return versato;
    }

    public Comanda versato(Double versato) {
        this.versato = versato;
        return this;
    }

    public void setVersato(Double versato) {
        this.versato = versato;
    }

    public Double getResto() {
        return resto;
    }

    public Comanda resto(Double resto) {
        this.resto = resto;
        return this;
    }

    public void setResto(Double resto) {
        this.resto = resto;
    }

    public Integer getCoperti() {
        return coperti;
    }

    public Comanda coperti(Integer coperti) {
        this.coperti = coperti;
        return this;
    }

    public void setCoperti(Integer coperti) {
        this.coperti = coperti;
    }

    public Integer getOmaggio() {
        return omaggio;
    }

    public Comanda omaggio(Integer omaggio) {
        this.omaggio = omaggio;
        return this;
    }

    public void setOmaggio(Integer omaggio) {
        this.omaggio = omaggio;
    }

    public String getNote() {
        return note;
    }

    public Comanda note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Cassa getCassa() {
        return cassa;
    }

    public Comanda cassa(Cassa cassa) {
        this.cassa = cassa;
        return this;
    }

    public void setCassa(Cassa cassa) {
        this.cassa = cassa;
    }

    public Set<DettaglioComanda> getDettaglioComandas() {
        return dettaglioComandas;
    }

    public Comanda dettaglioComandas(Set<DettaglioComanda> dettaglioComandas) {
        this.dettaglioComandas = dettaglioComandas;
        return this;
    }

    public Comanda addDettaglioComanda(DettaglioComanda dettaglioComanda) {
        this.dettaglioComandas.add(dettaglioComanda);
        dettaglioComanda.setComanda(this);
        return this;
    }

    public Comanda removeDettaglioComanda(DettaglioComanda dettaglioComanda) {
        this.dettaglioComandas.remove(dettaglioComanda);
        dettaglioComanda.setComanda(null);
        return this;
    }

    public void setDettaglioComandas(Set<DettaglioComanda> dettaglioComandas) {
        this.dettaglioComandas = dettaglioComandas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Boolean getTavolo() {
        return tavolo;
    }

    public LocalDateTime getConsegnato() {
        return consegnato;
    }

    public void setConsegnato(LocalDateTime consegnato) {
        this.consegnato = consegnato;
    }

    public LocalDateTime getPreparazione() {
        return preparazione;
    }

    public void setPreparazione(LocalDateTime preparazione) {
        this.preparazione = preparazione;
    }

    public LocalDateTime getRitirato() {
        return ritirato;
    }

    public void setRitirato(LocalDateTime ritirato) {
        this.ritirato = ritirato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comanda comanda = (Comanda) o;
        if (comanda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comanda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comanda{" +
            "id=" + getId() +
            ", progressivo=" + getProgressivo() +
            ", dataOra='" + getDataOra() + "'" +
            ", tavolo='" + isTavolo() + "'" +
            ", totale=" + getTotale() +
            ", versato=" + getVersato() +
            ", resto=" + getResto() +
            ", coperti=" + getCoperti() +
            ", omaggio=" + getOmaggio() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
