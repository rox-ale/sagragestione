package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * A Cassa.
 */
@Entity
@Table(name = "ingrediente")
public class Ingrediente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "griglia")
    private Boolean griglia;

    @OneToMany (mappedBy = "ingrediente")
   // @JsonIgnoreProperties("ingrediente")
    private Set< Ricetta> ricetta=new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Ingrediente nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public Boolean getGriglia() {
        return griglia;
    }

    public void setGriglia(Boolean griglia) {
        this.griglia = griglia;
    }

    public Set<Ricetta> getRicetta() {
        return ricetta;
    }

    public void setRicetta(Set<Ricetta> ricetta) {
        this.ricetta = ricetta;
    }

    @Override
    public String toString() {
        return "Ingrediente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", griglia=" + griglia +

                '}';
    }


}
