package it.alessiorossato.sagriamo.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Reparto.
 */
@Entity
@Table(name = "reparto")
public class Reparto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "abilitato")
    private Boolean abilitato;


    @OneToOne(fetch = FetchType.LAZY, optional = false)
  //  @JoinColumn(name = "reparto_id", nullable = false)
    private Stampante stampante;

    @OneToMany(mappedBy = "reparto")
    private Set<Categoria> categoria = new HashSet<>();

    @ManyToMany(mappedBy = "repartos")
    private Set<Cassa> cassas = new HashSet<>();

    public Reparto(String nome, Boolean abilitato, Stampante stampante, Set<Categoria> categoria) {
        this.nome = nome;
        this.abilitato = abilitato;
        this.stampante = stampante;
        this.categoria = categoria;
    }

    public Reparto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAbilitato() {
        return abilitato;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Stampante getStampante() {
        return stampante;
    }

    public void setStampante(Stampante stampante) {
        this.stampante = stampante;
    }

    public Set<Categoria> getCategoria() {
        return categoria;
    }

    public void setCategoria(Set<Categoria> categoria) {
        this.categoria = categoria;
    }

    public Set<Cassa> getCassas() {
        return cassas;
    }

    public void setCassas(Set<Cassa> cassas) {
        this.cassas = cassas;
    }
}

