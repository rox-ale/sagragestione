package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Categoria.
 */
@Entity
@Table(name = "categoria")
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "abilitato")
    private Boolean abilitato;

    @Column(name = "bar")
    private Boolean bar;

    @OneToMany(mappedBy = "categoria")
    private Set<Prodotto> prodottos = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    @ManyToOne
    private Reparto reparto;

    public Boolean getAbilitato() {
        return abilitato;
    }

    public Boolean getBar() {
        return bar;
    }

    public Reparto getReparto() {
        return reparto;
    }

    public void setReparto(Reparto reparto) {
        this.reparto = reparto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Categoria nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean isAbilitato() {
        return abilitato;
    }

    public Categoria abilitato(Boolean abilitato) {
        this.abilitato = abilitato;
        return this;
    }

    public void setAbilitato(Boolean abilitato) {
        this.abilitato = abilitato;
    }

    public Boolean isBar() {
        return bar;
    }

    public Categoria bar(Boolean bar) {
        this.bar = bar;
        return this;
    }

    public void setBar(Boolean bar) {
        this.bar = bar;
    }

    public Set<Prodotto> getProdottos() {
        return prodottos;
    }

    public Categoria prodottos(Set<Prodotto> prodottos) {
        this.prodottos = prodottos;
        return this;
    }

    public Categoria addProdotto(Prodotto prodotto) {
        this.prodottos.add(prodotto);
        prodotto.setCategoria(this);
        return this;
    }

    public Categoria removeProdotto(Prodotto prodotto) {
        this.prodottos.remove(prodotto);
        prodotto.setCategoria(null);
        return this;
    }

    public void setProdottos(Set<Prodotto> prodottos) {
        this.prodottos = prodottos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Categoria categoria = (Categoria) o;
        if (categoria.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), categoria.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Categoria{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", abilitato='" + isAbilitato() + "'" +
            ", bar='" + isBar() + "'" +
            "}";
    }
}
