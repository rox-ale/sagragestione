package it.alessiorossato.sagriamo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DettaglioComanda.
 */
@Entity
@Table(name = "ricetta")
public class Ricetta implements Serializable {

    private static final long serialVersionUID = 1L;
    
   /* @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
*/

    //@EmbeddedId
     /*
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)*/
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;


    @Column(name = "qta")
    private Integer qta;


    @ManyToOne
    @JoinColumn(name = "prodotto_id")
    private Prodotto prodotto;

    @ManyToOne
    @JoinColumn(name = "ingrediente_id")
    private Ingrediente ingrediente;

  /*  public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
*/
    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Ricetta{" +
                "id=" + id +
                ", qta=" + qta +
                ", prodotto=" + prodotto +
                ", ingrediente=" + ingrediente +
                '}';
    }
}
